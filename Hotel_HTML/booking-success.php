<?php include 'header.php'; ?>
<main class="container">
    <section class="fail hidden-sm hidden-xs">
        <div class="step text-center">
            <div class="col-md-4">
                <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                <p>Chọn khách sạn</p>
            </div>
            <div class="col-md-4">
                <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                <p style="max-width: 140px; display: inline-block;">Khách sạn của bạn & Thanh toán</p>
            </div>
            <div class="col-md-4">
                <p style="font-size: 30px; color: #fff; display: inline; background: #044079; border-radius: 50%; padding: 1px 9px;">3</p><br>
                <p style="max-width: 140px; display: inline-block;"> Đặt khách sạn thành công</p>
            </div>
        </div>
    </section>

    <section class="fail">
        <div class="content-fail">
            <div class="fail">
                <div class="fail2">
                    <div class="notice">
                        <img src="img/icon/booking-success.png" alt="booking fail icon"/> &nbsp;  <span>ĐẶT PHÒNG THÀNH CÔNG</span>
                    </div>
                    <div class="inner">
                        <p>Xin chân thành cảm ơn quý khách Nguyen Van An! Đặt phòng của bạn đã được thanh toán thành công. Vui lòng kiểm tra lại thông tin đặt phòng của bạn:</p>
                    </div>
                    <div class="suggest">
                        <div class="col-md-3 col-sm-2 max-xxs">
                            <div class="hotel">
                                <div class="name-hotel">REX HOTEL <i class="fa fa-star-o"></i></div>
                                <div class="img-hotel hidden-xs">
                                    <img class="img-responsive" src="img/hotel/69803_165_z.jpg" alt=""/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-6 max-xxs">
                            <div class="information-inner">
                                <div class="date-label">
                                    <div>
                                        09 <br />
                                        10/2015
                                    </div>
                                    <div>Ngày nhận phòng</div>
                                </div>
                                <ul>
                                    <li><span>Mã tham chiếu: </span> ABC-123456</li> 
                                    <li><span>Ngày nhận phòng: </span> 07/10/2015</li> 
                                    <li><span>Khách hàng: </span> TRAN VAN B</li> 
                                    <li><span>Khu vực: </span> Quận 1</li> 
                                    <li><span>Bữa ăn: </span> </li> 
                                    <li><span>Dịch vụ thêm: </span> </li> 
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-5 col-xs-6 max-xxs">
                            <div class="information-inner">
                                <div class="date-label">
                                    <div>
                                        09 <br />
                                        10/2015
                                    </div>
                                    <div>Ngày trả phòng</div>
                                </div>

                                <p><strong>Điều kiện hủy:</strong></p>
                                <p>2 ngày trước khi nhận phòng (10 tháng 10, 2015).</p>
                                <p>Hủy miễn phí trước ngày 10 tháng 10, 2015 - 12:00.</p>
                                <p>Hủy sau ngày 10 tháng 10, 2015 - 12:00 sẽ bị thu phí 1 đêm tiền phòng</p>
                            </div>
                        </div>
                    </div>
                    <div class="inner">
                        <p>Bạn sẽ nhận được email thông báo của chúng tôi trong vài phút. Vui lòng kiểm tra trong hộp thư của bạn (hoặc trong mục Spam). </p>
                        <p>Để biết thêm thông tin hoặc nếu có thắc mắc cần chúng tôi giải quyết, xin quý khách đừng ngần ngại liên hệ với chúng tôi:</p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!-- Flight Booking Suggestion -->

    <section class="fail">
        <div class="book-hotel">
            <form class="form-horizontal">
                <div class="name-book">GỢI Ý ĐẶT VÉ MÁY BAY ĐI ĐÀ NẴNG</div>
                <div class="form-book search-form row">

                    <div class="col-md-7">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="control-label col-md-4">Bay từ</label>
                                    <div class="col-md-8">
                                        <div class="input-wrap">
                                            <input class="form-control" name="" value="Ho Chi Minh (SGN)" type="text"><i class="fa fa-angle-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="control-label col-md-4">Bay đến</label>
                                    <div class="col-md-8">
                                        <div class="input-wrap">
                                            <input class="form-control" name="" value="Ha Noi (SGN)" type="text"><i class="fa fa-angle-down"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-xs-4">
                                <div class="row form-group">
                                    <label class="control-label col-md-6">Ng lớn</label>
                                    <div class="col-md-6">
                                        <div class="input-wrap">
                                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="row form-group">
                                    <label class="control-label col-md-6">Trẻ em</label>
                                    <div class="col-md-6">
                                        <div class="input-wrap">
                                            <i class="fa fa-minus"></i><input class="form-control" name="" value="0" type="text"><i class="fa fa-plus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-4">
                                <div class="row form-group">
                                    <label class="control-label col-md-6">Em bé</label>
                                    <div class="col-md-6">
                                        <div class="input-wrap">
                                            <i class="fa fa-minus"></i><input class="form-control" name="" value="0" type="text"><i class="fa fa-plus"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-5">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="control-label col-md-5">Ngày đi</label>
                                    <div class="col-md-7">
                                        <div class="input-wrap">
                                            <input class="form-control calendar" value="15/07/2016" name="" type="text"><i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group row">
                                    <label class="control-label col-md-5">Ngày về</label>
                                    <div class="col-md-7">
                                        <div class="input-wrap">
                                            <input class="form-control calendar" value="15/07/2017" name="" type="text"><i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button class="btn btn-primary form-control">TÌM KIẾM NGAY</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <a href="#" class="pull-right">Tìm kiếm nâng cao</a>
                    </div>

                </div>
            </form>
        </div>
    </section>

    <!-- End Flight Booking Suggestion -->




    <!-- Tour Booking Suggestion -->

    <section class="tour-suggestion">

        <h2 class="text-center tour-suggestion-title">
            Bạn sắp đặt chân đến ĐÀ NẴNG. Bạn có kế hoạch gì cho một chuyết đi thú vị?
            <span>GỢI Ý TOURS TẠI ĐÀ NẴNG</span>
        </h2>

        <div class="row">
            <div class="col-md-3 col-sm-6 item">
                <div class="inner">
                    <div class="img-wrap">
                        <img src="img/tour/1.jpg" class="img-responsive" alt=""/>
                    </div>
                    <a href="#"><h4>LĂNG CÔ - ĐÀ NẴNG</h4></a>
                    <div class="star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="price">Giá từ VND <span>1.550.000</span></div>
                    <div class="tax-info">(Giá đã bao gồm VAT)</div>
                    <a href="#" class="btn btn-success text-uppercase">Đặt ngay</a>
            </div>
            <div class="col-md-3 col-sm-6  item">
                <div class="inner">
                    <div class="img-wrap">
                        <img src="img/tour/2.jpg" class="img-responsive" alt=""/>
                    </div>
                    <a href="#"><h4>BÀ NÀ HILL</h4></a>
                    <div class="star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="price">Giá từ VND <span>1.550.000</span></div>
                    <div class="tax-info">(Giá đã bao gồm VAT)</div>
                    <a href="#" class="btn btn-success text-uppercase">Đặt ngay</a>
            </div>
            <div class="col-md-3 col-sm-6  item">
                <div class="inner">
                    <div class="img-wrap">
                        <img src="img/tour/3.jpg" class="img-responsive" alt=""/>
                    </div>
                    <a href="#"><h4>ĐẢO XANH LÝ SƠN</h4></a>
                    <div class="star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="price">Giá từ VND <span>1.550.000</span></div>
                    <div class="tax-info">(Giá đã bao gồm VAT)</div>
                    <a href="#" class="btn btn-success text-uppercase">Đặt ngay</a>
            </div>
            <div class="col-md-3 col-sm-6  item">
                <div class="inner">
                    <div class="img-wrap">
                        <img src="img/tour/4.jpg" class="img-responsive" alt=""/>
                    </div>
                    <a href="#"><h4>VÒNG QUAY KHỔNG LỒ</h4></a>
                    <div class="star">
                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                        </div>
                    </div>
                    <div class="price">Giá từ VND <span>1.550.000</span></div>
                    <div class="tax-info">(Giá đã bao gồm VAT)</div>
                    <a href="#" class="btn btn-success text-uppercase">Đặt ngay</a>
            </div>
        </div>
    </section>

    <!-- End Tour Booking Suggestion -->


</main>


<?php include 'footer.php'; ?>