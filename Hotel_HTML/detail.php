<?php require_once 'header.php'; ?>

<!-- Main start -->

<!--------------------------------

---------------------------------->
<main class="container">
    
    <!-- Search bar -->
    <div class="search-bar search-form">
        <div class="row">

            <div class="form-group col-xs-12 col-md-2">
                <label>Điểm đến</label>
                <div class="input-wrap">
                    <input class="form-control" name="" value="Ho Chi Minh (SGN)" type="text"><i class="fa fa-angle-down"></i>
                </div>
            </div>
            <div class="form-group col-xs-6 col-md-2">
                <label>Ngày đi</label>
                <div class="input-wrap">
                    <input class="form-control calendar" value="15/07/2016" name="" type="text"><i class="fa fa-calendar"></i>
                </div>
            </div>
            <div class="form-group col-xs-6 col-md-2">
                <label>Ngày về</label>
                <div class="input-wrap">
                    <input class="form-control calendar" value="15/07/2016" name="" type="text"><i class="fa fa-calendar"></i>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <div class="row">
                    <div class="col-xs-4">
                        <label>Người lớn</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label>Trẻ em</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label>Số phòng</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 item">
                <button class="btn btn-block btn-primary">Tìm kiếm mới</button>
            </div>
        </div>
    </div>
    <!-- Search bar -->

    <!-- General Info -->
    <div class="general-info">
        <div class="pull-left">
            <h1 class="title">rex hotel</h1>
            <p>141 Nguyễn Huệ, Quận 1, TP. Hồ Chí Minh, Việt Nam</p>
            <p>Email hotel : rexhotel@gmail.com   -  Website : rexhotel.com</p>
            <p>Hotline: <strong>123456789</strong></p>
            <a href="#">Xem bản đồ <i class="fa fa-map-marker"></i></a>
        </div>
        <div class="pull-right text-right">
            <p>Giá chỉ từ</p>
            <p><span class="price">1.990.000 VND </span> / ĐÊM</p>
            <p><span class="heart">&#10084;</span> Lưu vào danh sách yêu thích</p>
            <button class="btn btn-block btn-default">Đặt ngay</button>
        </div>
    </div>
    <!-- General Info end -->

    <div class="row hotel-main-info">
        <div class="col-lg-10">
            <!-- Main info -->
            <div class="main-info">
                <div class="row">
                    <div class="col-md-8">
                        <div class="inner">
                            <div id="jssor_1" style="position: relative; margin: 0 auto; top: 0px; left: 0px; width: 800px; height: 456px; overflow: hidden; visibility: hidden; background-color: #24262e;">
                                <!-- Loading Screen -->
                                <div data-u="loading" style="position: absolute; top: 0px; left: 0px;">
                                    <div style="filter: alpha(opacity=70); opacity: 0.7; position: absolute; display: block; top: 0px; left: 0px; width: 100%; height: 100%;"></div>
                                    <div style="position:absolute;display:block;background:url('img/loading.gif') no-repeat center center;top:0px;left:0px;width:100%;height:100%;"></div>
                                </div>
                                <div data-u="slides" style="cursor: default; position: relative; top: 0px; left: 0px; width: 800px; height: 356px; overflow: hidden;">
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/01.jpg" />
                                        <img data-u="thumb" src="img/thumb-01.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/02.jpg" />
                                        <img data-u="thumb" src="img/thumb-02.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/03.jpg" />
                                        <img data-u="thumb" src="img/thumb-03.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/04.jpg" />
                                        <img data-u="thumb" src="img/thumb-04.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/05.jpg" />
                                        <img data-u="thumb" src="img/thumb-05.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/06.jpg" />
                                        <img data-u="thumb" src="img/thumb-06.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/07.jpg" />
                                        <img data-u="thumb" src="img/thumb-07.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/08.jpg" />
                                        <img data-u="thumb" src="img/thumb-08.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/09.jpg" />
                                        <img data-u="thumb" src="img/thumb-09.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/10.jpg" />
                                        <img data-u="thumb" src="img/thumb-10.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/11.jpg" />
                                        <img data-u="thumb" src="img/thumb-11.jpg" />
                                    </div>
                                    <div data-p="144.50" style="display: none;">
                                        <img data-u="image" src="img/12.jpg" />
                                        <img data-u="thumb" src="img/thumb-12.jpg" />
                                    </div>
                                </div>
                                <!-- Thumbnail Navigator -->
                                <div data-u="thumbnavigator" class="jssort01" style="position:absolute;left:0px;bottom:0px;width:800px;height:100px;" data-autocenter="1">
                                    <!-- Thumbnail Item Skin Begin -->
                                    <div data-u="slides" style="cursor: default;">
                                        <div data-u="prototype" class="p">
                                            <div class="w">
                                                <div data-u="thumbnailtemplate" class="t"></div>
                                            </div>
                                            <div class="c"></div>
                                        </div>
                                    </div>
                                    <!-- Thumbnail Item Skin End -->
                                </div>
                                <!-- Arrow Navigator -->
                                <span data-u="arrowleft" class="jssora05l" style="top:158px;left:8px;width:40px;height:40px;"></span>
                                <span data-u="arrowright" class="jssora05r" style="top:158px;right:8px;width:40px;height:40px;"></span>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 key-point">
                        <div class="h3-title text-primary">Đặc điểm nổi bật</div>
                        <div class="review-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-empty"></i>

                        </div>
                        <ul class="list-unstyled ul-title">
                            <li><span class="text-success"><i class="fa fa-train"></i> <strong>Nhà ga</strong></span> Gas Sài Gòn (2.8km)</li>
                            <li><span class="text-success"><i class="fa fa-bank"></i> <strong>Bảo tàng</strong></span>
                                <ul>
                                    <li>Bảo tàng lịch sử Việt Nam (3 Km)</li>
                                    <li>Bảo tàng Thành phố Hồ Chí Minh (2 Km)</li>
                                    <li>Bảo tàng Tôn Đức Thắng (3,9 Km)</li>
                                    <li>Bảo tàng Mỹ thuật (5 Km)</li>
                                </ul>
                            </li>
                            <li>
                                <span class="text-success"><i class="fa fa-plane"></i> <strong>Sân bay</strong></span>
                                <ul>
                                    <li>Sân bay Quốc Tế Tân Sơn Nhất (6 Km)</li>
                                    <li>Sân bay Quốc Tế Long Thành (25.1 Km)</li>
                                </ul>
                            </li>
                            <li><span class="text-success"><i class="fa fa-cart-arrow-down"></i> <strong>Trung tâm thương mại</strong></span>
                                <ul>
                                    <li>Trung tâm thương mại Eden (01 Km)</li>
                                    <li>Thương xá Tax (01 Km</li>
                                </ul>

                            </li>
                        </ul>

                    </div>
                </div>

                <!-- Short introduction -->
                <div class="row short-introduction">
                    <div class="col-xs-12">
                        <p>Rex Hotel Sài Gòn nằm ở khu trung tâm Thành phố Hồ Chí Minh, cách Chợ Bến Thành 1 km. Khách sạn có sòng bạc và nhà hàng trên tầng mái. Phòng tập thể dục và spa đủ dịch vụ cũng có tại đây.</p>
                        <p>Khách sạn Rex Hotel cách Nhà hát Múa rối Nước Rồng Vàng và Nhà hát lớn Thành phố 10 phút đi bộ. Nhà thờ Đức Bà cách khách sạn 500 m.</p>
                        <p>Các phòng nghỉ tại đây có phong cách trang trí hiện đại kiểu Việt Nam và đi kèm truyền hình cáp, tiện nghi pha trà/cà phê cũng như truy cập Wi-Fi miễn phí.</p>
                        <p>Quý khách có thể thư giãn bằng cách bơi trong hồ bơi hoặc chơi trò tennis. Các lớp học yoga, thể dục nhịp điệu và khiêu vũ cũng được tổ chức tại đây. Ngoài ra, khách sạn còn có bồn tắm nước nóng.</p>
                        <p>Nhà hàng Rooftop Garden phục vụ ẩm thực Trung Hoa và Pháp."</p>
                        <p>Quận 1 là lựa chọn tuyệt vời cho du khách thích giải trí về đêm, khu chợ và mua sắm</p>
                    </div>
                </div>
                <!-- Short introduction end -->

                <!-- Available Rooms -->
                <div class="available-rooms">
                    <div class="h2-title select-room-title">Xin mời quý khách chọn phòng</div>

                    <div class="row">
                        <div class="col-md-10">
                            <ul class="list-unstyled list-room">
                                <?php for ($i = 0; $i < 5; $i++) : ?>
                                    <li>
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                            </div>
                                            <div class="col-sm-8" style="height:100%;">
                                                <div class="row" style="height:100%;">
                                                    <div class="col-sm-8 room-info">
                                                        <h3><a href="#">Family Suite</a></h3>
                                                        <p>45 m<sup>2</sup> Chúng tôi còn <strong class="text-success">02</strong> phòng</p>
                                                        <ul class="list-inline">
                                                            <li class="a-001"> x1</li>
                                                            <li class="a-002"> x2</li>
                                                            <li class="a-003"> x3</li>
                                                        </ul>
                                                        <ul class="list-inline bottom hidden-xs">
                                                            <li class="a-004"></li>
                                                            <li class="a-005"></li>
                                                            <li class="a-006"></li>
                                                            <li class="a-007"></li>
                                                            <li class="a-008"></li>
                                                            <li class="a-009"></li>
                                                            <li class="a-010"></li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-sm-4 price-info">
                                                        <div class="price">VND <span class="text-success">1.750.000</span></div>
                                                        <div class="text-primary">Giảm giá 30%</div>
                                                        <button class="btn btn-block">Đặt ngay</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                <?php endfor; ?>

                            </ul>
                            <button class="btn btn-block btn-success load-more-room">Tải thêm danh sách phòng</button>
                        </div>
                        <div class="col-md-2 no-padding-left hidden-xs list-facilities-wrap">
                            <div class="list-facilities">
                                <div class="h3-title text-center">
                                    Tiện nghi khách sạn

                                </div>
                                <ul class="list-unstyled list-items">
                                    <li class="a-001">Free Wifi</li>
                                    <li class="a-002">Free Breakfast</li>
                                    <li class="a-003">Iron</li>
                                    <li class="a-004">Heater</li>
                                    <li class="a-005">Bar Wine</li>
                                    <li class="a-006">Swimming pool</li>
                                    <li class="a-007">Cooking</li>
                                    <li class="a-008">Fridge</li>
                                    <li class="a-009">Bath tub shower</li>
                                    <li class="a-010">Televison</li>
                                    <li class="a-001">Air conditional</li>
                                    <li class="a-002">Fan</li>
                                    <li class="a-003">Hairdryer</li>
                                    <li class="a-004">Safe</li>
                                    <li class="a-005">Massage Chair</li>
                                    <li class="a-006">Swimming pool</li>
                                    <li class="a-007">Cooking</li>
                                    <li class="a-008">Fridge</li>
                                    <li class="a-009">Bath tub shower</li>
                                    <li class="a-010">Televison</li>
                                    <li class="a-001">Air conditional</li>
                                    <li class="a-002">Fan</li>
                                    <li class="a-003">Hairdryer</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Available Rooms End -->



                <!-- Facility -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="facility">
                            <div class="pull-left text-primary">
                                Các tiện nghi của <span>Rex hotel</span>
                            </div>
                            <div class="pull-right">
                                Đánh giá: <span class="text-primary">Tiện nghi tuyệt vời</span> <span class="text-success">8,6</span>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row list-facilities">
                    <div class="col-md-4">
                        <ul class="group">
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="group">
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <ul class="group">
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                            <li class="title">
                                Ngoài trời
                                <ul>
                                    <li>Hồ bơi ngoài trời</li>
                                    <li>Sân vườn</li>
                                    <li>Sân thương / Hiên</li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Facility End -->

            </div>
        </div>
        <!-- main info end -->
        <div class="col-lg-2 hidden-xs hidden-sm">
            <div class="right-sidebar">
                <div class="h3-title text-primary">Khách sạn đã xem</div>
                <button class="btn btn-block"><i class="fa fa-angle-up"></i></button>
                <ul class="list-hotel list-unstyled">
                    <li>
                        <a href="#"><img src="img/hotel.jpg" class="img-responsive" alt=""/></a>
                        <a class="text-primary" href="#">Hotel Nikko Saigon</a>

                        <div class="review-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-empty"></i>
                        </div>
                        <div class="price">
                            Từ: <span>VND 10,500,000</span>
                        </div>
                        <div class="viewing">29 người đang xem</div>
                    </li>
                    <li>
                        <a href="#"><img src="img/hotel.jpg" class="img-responsive" alt=""/></a>
                        <a class="text-primary" href="#">Hotel Nikko Saigon</a>

                        <div class="review-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-empty"></i>
                        </div>
                        <div class="price">
                            Từ: <span>VND 10,500,000</span>
                        </div>
                        <div class="viewing">29 người đang xem</div>
                    </li>
                    <li>
                        <a href="#"><img src="img/hotel.jpg" class="img-responsive" alt=""/></a>
                        <a class="text-primary" href="#">Hotel Nikko Saigon</a>

                        <div class="review-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-empty"></i>
                        </div>
                        <div class="price">
                            Từ: <span>VND 10,500,000</span>
                        </div>
                        <div class="viewing">29 người đang xem</div>
                    </li>
                    <li>
                        <a href="#"><img src="img/hotel.jpg" class="img-responsive" alt=""/></a>
                        <a class="text-primary" href="#">Hotel Nikko Saigon</a>

                        <div class="review-star">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-empty"></i>
                        </div>
                        <div class="price">
                            Từ: <span>VND 10,500,000</span>
                        </div>
                        <div class="viewing">29 người đang xem</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</main>
<!-- Main End -->

<!-- Popular hotel -->
<div class="popular-hotel hidden-xs hidden-sm">
    <div class="container">
        <div class="inner">
            <h2>Khách sạn phổ biến tại Tp. Hồ Chí Minh</h2>
            <div class="hotel-items row">

                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Popular hotel End-->



<script src="js/jssor.slider.debug.js" type="text/javascript"></script>


<!-- use jssor.slider.debug.js instead for debug -->
<script>
    jQuery(document).ready(function ($) {

        var jssor_1_SlideshowTransitions = [
            {$Duration: 1200, x: 0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: -0.3, $SlideOut: true, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: -0.3, $During: {$Left: [0.3, 0.7]}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, $SlideOut: true, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: 0.3, $During: {$Top: [0.3, 0.7]}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: -0.3, $SlideOut: true, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: -0.3, $During: {$Top: [0.3, 0.7]}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: 0.3, $SlideOut: true, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, $Cols: 2, $During: {$Left: [0.3, 0.7]}, $ChessMode: {$Column: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, $Cols: 2, $SlideOut: true, $ChessMode: {$Column: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: 0.3, $Rows: 2, $During: {$Top: [0.3, 0.7]}, $ChessMode: {$Row: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: 0.3, $Rows: 2, $SlideOut: true, $ChessMode: {$Row: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: 0.3, $Cols: 2, $During: {$Top: [0.3, 0.7]}, $ChessMode: {$Column: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, y: -0.3, $Cols: 2, $SlideOut: true, $ChessMode: {$Column: 12}, $Easing: {$Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, $Rows: 2, $During: {$Left: [0.3, 0.7]}, $ChessMode: {$Row: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: -0.3, $Rows: 2, $SlideOut: true, $ChessMode: {$Row: 3}, $Easing: {$Left: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $ChessMode: {$Column: 3, $Row: 12}, $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, x: 0.3, y: 0.3, $Cols: 2, $Rows: 2, $During: {$Left: [0.3, 0.7], $Top: [0.3, 0.7]}, $SlideOut: true, $ChessMode: {$Column: 3, $Row: 12}, $Easing: {$Left: $Jease$.$InCubic, $Top: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, $Delay: 20, $Clip: 3, $Assembly: 260, $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, $Delay: 20, $Clip: 3, $SlideOut: true, $Assembly: 260, $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, $Delay: 20, $Clip: 12, $Assembly: 260, $Easing: {$Clip: $Jease$.$InCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2},
            {$Duration: 1200, $Delay: 20, $Clip: 12, $SlideOut: true, $Assembly: 260, $Easing: {$Clip: $Jease$.$OutCubic, $Opacity: $Jease$.$Linear}, $Opacity: 2}
        ];

        var jssor_1_options = {
            $AutoPlay: true,
            $SlideshowOptions: {
                $Class: $JssorSlideshowRunner$,
                $Transitions: jssor_1_SlideshowTransitions,
                $TransitionsOrder: 1
            },
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 10,
                $SpacingX: 8,
                $SpacingY: 8,
                $Align: 360
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        //responsive code begin
        //you can remove responsive code if you don't want the slider scales while window resizes
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 800);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $(window).bind("load", ScaleSlider);
        $(window).bind("resize", ScaleSlider);
        $(window).bind("orientationchange", ScaleSlider);
        //responsive code end
    });
</script>

<style>

    /* jssor slider arrow navigator skin 05 css */
    /*
    .jssora05l                  (normal)
    .jssora05r                  (normal)
    .jssora05l:hover            (normal mouseover)
    .jssora05r:hover            (normal mouseover)
    .jssora05l.jssora05ldn      (mousedown)
    .jssora05r.jssora05rdn      (mousedown)
    */
    .jssora05l, .jssora05r {
        display: block;
        position: absolute;
        /* size of arrow element */
        width: 40px;
        height: 40px;
        cursor: pointer;
        background: url('img/a17.png') no-repeat;
        overflow: hidden;
    }
    .jssora05l { background-position: -10px -40px; }
    .jssora05r { background-position: -70px -40px; }
    .jssora05l:hover { background-position: -130px -40px; }
    .jssora05r:hover { background-position: -190px -40px; }
    .jssora05l.jssora05ldn { background-position: -250px -40px; }
    .jssora05r.jssora05rdn { background-position: -310px -40px; }

    /* jssor slider thumbnail navigator skin 01 css */
    /*
    .jssort01 .p            (normal)
    .jssort01 .p:hover      (normal mouseover)
    .jssort01 .p.pav        (active)
    .jssort01 .p.pdn        (mousedown)
    */
    .jssort01 .p {
        position: absolute;
        top: 0;
        left: 0;
        width: 72px;
        height: 72px;
    }

    .jssort01 .t {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        border: none;
    }

    .jssort01 .w {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 100%;
        height: 100%;
    }

    .jssort01 .c {
        position: absolute;
        top: 0px;
        left: 0px;
        width: 68px;
        height: 68px;
        border: #000 2px solid;
        box-sizing: content-box;
        background: url('img/t01.png') -800px -800px no-repeat;
        _background: none;
    }

    .jssort01 .pav .c {
        top: 2px;
        _top: 0px;
        left: 2px;
        _left: 0px;
        width: 68px;
        height: 68px;
        border: #000 0px solid;
        _border: #fff 2px solid;
        background-position: 50% 50%;
    }

    .jssort01 .p:hover .c {
        top: 0px;
        left: 0px;
        width: 70px;
        height: 70px;
        border: #fff 1px solid;
        background-position: 50% 50%;
    }

    .jssort01 .p.pdn .c {
        background-position: 50% 50%;
        width: 68px;
        height: 68px;
        border: #000 2px solid;
    }

    * html .jssort01 .c, * html .jssort01 .pdn .c, * html .jssort01 .pav .c {
        /* ie quirks mode adjust */
        width /**/: 72px;
        height /**/: 72px;
    }

</style>

<?php require 'footer.php'; ?>