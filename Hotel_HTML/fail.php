<?php include 'header.php'; ?>
<main class="container">
    <section class="fail hidden-sm hidden-xs">
        <div class="step text-center">
            <div class="col-md-4">
                <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                <p>Chọn khách sạn</p>
            </div>
            <div class="col-md-4">
                <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                <p style="max-width: 140px; display: inline-block;">Khách sạn của bạn & Thanh toán</p>
            </div>
            <div class="col-md-4">
                <p style="font-size: 30px; color: #fff; display: inline; background: #044079; border-radius: 50%; padding: 1px 9px;">3</p><br>
                <p style="max-width: 140px; display: inline-block;"> Đặt khách sạn thành công</p>
            </div>
        </div>
    </section>

    <section class="fail">
        <div class="content-fail">
            <div class="fail">
                <div class="fail2">
                    <div class="notice">
                        <img src="img/icon/booking-fail.png" alt="booking fail icon"/> &nbsp;  ĐẶT PHÒNG THẤT BẠI
                    </div>
                    <div class="inner">
                        <p>Xin chân thành cảm ơn quý khách Nguyen Van An đã sử dụng dịch vụ của chúng tôi!<br>
                            Do một số lý do, chúng tôi không thể xác nhận booking của bạn. Bộ phận hỗ trợ khách hàng của chúng tôi sẽ sớm liên hệ với bạn.</p>
                        <p>Bạn có thể tham khảo thông tin liên quan đến booking không thành công bên dưới.</p>
                    </div>
                    <div class="suggest">
                        <div class="row">
                            <div class="col-sm-5 col-md-3">
                                <div class="hotel">
                                    <div class="name-hotel">REX HOTEL <i class="fa fa-star"></i> <i class="fa fa-star-half-o"></i> <i class="fa fa-star-o"></i></div>
                                    <div class="img-hotel">
                                        <img class="img-responsive" src="img/hotel/69803_165_z.jpg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-7">
                                <div class="check-in">
                                    <div class="row date-check">
                                        <div class="date-check-in col-xs-6">
                                            <div style="float: left; max-width: 100px; border-radius: 15px; border: 2px solid #044079; text-align: center;margin-right: 5px;display: inline-block;" class="date">
                                                10 09/2015
                                            </div>
                                            <div>
                                                Ngày nhận phòng
                                            </div>
                                        </div>

                                        <div class="date-check-out col-xs-6">
                                            <div style="float: left; max-width: 100px; border-radius: 15px; border: 2px solid #044079; text-align: center;margin-right: 5px;display: inline-block;" class="date">10 09/2015</div>
                                            <div>
                                                Ngày trả phòng
                                            </div>
                                        </div>
                                    </div>


                                    <div class="group-about">
                                        <div class="name-about">
                                            MÃ SỐ THAM CHIẾU: 
                                        </div>
                                        <div class="about1">
                                            BB0915-AA01234
                                        </div>
                                    </div>
                                    <div class="group-about">
                                        <div class="name-about">
                                            MÃ SỐ ĐẶT CHỖ:
                                        </div>
                                        <div class="about1">XI-HF012234573</div>
                                    </div>
                                    <div class="group-about">
                                        <div class="name-about">
                                            Khách đặt phòng:
                                        </div>
                                        <div class="about2">TRAN VAN B</div>
                                    </div>
                                    <div class="group-about">
                                        <div class="name-about">
                                            Chi tiết:
                                        </div>
                                        <div class="about2">
                                            3 Phòng : Standar, Premium,<br>
                                            Family Suite<br>
                                            2 Đêm<br>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 col-sm-12">
                                <div class="check-in">
                                    <div class="group-about">
                                        <div class="name-about">
                                            Cổng thanh toán: 
                                        </div>
                                        <div class="about2">SMARTLINK</div>
                                    </div>
                                    <div class="group-about">
                                        <div class="name-about">
                                            Tình trạng:
                                        </div>
                                        <div class="about3">KHÔNG THÀNH CÔNG</div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- End row -->
                    </div>
                    <div class="inner">
                        <p>Bạn sẽ nhận được email thông báo của chúng tôi trong vài phút. Vui lòng kiểm tra trong hộp thư của bạn (hoặc trong mục Spam). </p>
                        <p>Để biết thêm thông tin hoặc nếu có thắc mắc cần chúng tôi giải quyết, xin quý khách đừng ngần ngại liên hệ với chúng tôi:</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="fail">
        <div class="book-hotel">
            <div class="name-book">GỢI Ý ĐẶT VÉ MÁY BAY ĐI ĐÀ NẴNG</div>
            <div class="form-book">
                <div class="col-md-3">
                    <label>Bay từ: </label>
                    <select class="form-control form-group">
                        <option>Hà nợi</option>
                        <option>Hồ Chí Minh</option>
                        <option>Đà Nẵng</option>
                        <option>Buôn Ma Thuột</option>
                        <option>Cần Thơ</option>
                    </select>
                    <div class="search-advanced">
                        Tìm kiếm nâng cao
                    </div>
                </div>
                <div class="col-md-3">
                    <label>Nơi đến</label>
                    <select class="form-control form-group">
                        <option>Hà nợi</option>
                        <option>Hồ Chí Minh</option>
                        <option>Đà Nẵng</option>
                        <option>Buôn Ma Thuột</option>
                        <option>Cần Thơ</option>
                    </select>

                    <label>Người lớn</label>
                    <select class="form-control form-group">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Ngày đi</label>
                    <input class="form-control form-group" type="date" />

                    <label>Trẻ em</label>
                    <select class="form-control form-group">
                        <option>1</option>
                        <option>2</option>
                        <option>3</option>
                        <option>4</option>
                        <option>5</option>
                    </select>
                </div>
                <div class="col-md-3">
                    <label>Ngày về</label>
                    <input class="form-control form-group" type="date" />
                    <button class="btn btn-primary form-control">TÌM KIẾM NGAY</button>
                </div>
            </div>
        </div>
    </section>
</main>


<?php include 'footer.php'; ?>