<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/bootstrap-theme.min.css">
        <link href="vendor/font-awesome-4.4.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/owl-carousel/owl.carousel.css" rel="stylesheet" type="text/css"/>
        <link href="css/icon.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/owl-carousel/owl.theme.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/owl-carousel/owl.transitions.css" rel="stylesheet" type="text/css"/>
        <link href="vendor/zurb-responsive-tables-0d34bc6/responsive-tables.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="css/main.css">
        
        
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Topbar -->
        <div class="topbar hidden-xs">
            <div class="container">
                <div class="pull-right">
                    <div class="support-number hidden-xs">
                        <span class="inner">
                            <i class="fa fa-phone"></i> 1900 9002 - (08) 62 850 850
                        </span>
                    </div>
                    <div class="clock hidden-xs">
                        <i class="fa fa-clock-o"></i>
                    </div>
                    <div class="account">
                        <div class="inner">
                            <span>Đăng nhập <br />Tài khoản</span>
                        </div>
                    </div>
                    <div class="lang-select">
                        <span class="inner">
                            <img src="img/flag-vn.png" alt=""/>
                        </span>
                    </div>
                </div>
            </div> 
        </div>
        <!-- Topbar end -->

        <!-- Main nav start -->
             <!-- Main nav start -->
        <nav class="navbar navbar-default main-nav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">
                        <img src="img/logo.png" alt=""/>
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#">Trang chủ</a></li>
                        <li><a href="#">Free and Easy</a></li>
                        <li><a href="#">Tour</a></li>
                        <li><a href="#">Điểm đến</a></li>
                        <li><a href="#">Tạp chí</a></li>
                        <li><a href="#">Hợp tác</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>


    <!-- Main nav end -->

    <!-- Breadbar star -->
    <div class="bread-bar">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="#">Trang chủ</a></li>
                <li><span>Kết quả tìm khách sạn</span></li>
            </ul>
            <ul class="currency-and-list list-unstyled">
                <li class="hidden-xs"><a href="#">VND</a></li>
                <li class="hidden-xs"><a href="#">Mới xem gần đây</a></li>
                <li class="hidden-xs"><a href="#"><span class="heart">&#10084;</span>Danh sách của bạn</a></li>
                <li><a id="search-button" href="#"><i class="fa fa-search"></i></a></li>
            </ul>
            
        </div>

    </div>

    <!-- Breadbar End -->