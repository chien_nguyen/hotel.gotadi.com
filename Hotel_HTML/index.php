<?php include 'header.php'; ?>

<!-- Main start -->
<main class="container">

    <!-- Search bar -->
    <div class="search-bar search-form">
        <div class="row">

            <div class="form-group col-xs-12 col-md-2">
                <label>Điểm đến</label>
                <div class="input-wrap">
                    <input class="form-control" name="" value="Ho Chi Minh (SGN)" type="text"><i class="fa fa-angle-down"></i>
                </div>
            </div>
            <div class="form-group col-xs-6 col-md-2">
                <label>Ngày đi</label>
                <div class="input-wrap">
                    <input class="form-control calendar" value="15/07/2016" name="" type="text"><i class="fa fa-calendar"></i>
                </div>
            </div>
            <div class="form-group col-xs-6 col-md-2">
                <label>Ngày về</label>
                <div class="input-wrap">
                    <input class="form-control calendar" value="15/07/2016" name="" type="text"><i class="fa fa-calendar"></i>
                </div>
            </div>
            <div class="form-group col-xs-12 col-md-4">
                <div class="row">
                    <div class="col-xs-4">
                        <label>Người lớn</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label>Trẻ em</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <label>Số phòng</label>
                        <div class="input-wrap">
                            <i class="fa fa-minus"></i><input class="form-control" name="" value="1" type="text"><i class="fa fa-plus"></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-2 item">
                <button class="btn btn-block btn-primary">Tìm kiếm mới</button>
            </div>
        </div>
    </div>
    <!-- Search bar -->

    <div class="row">


        <!-- Main list start -->
        <div class="col-md-9 col-md-push-3">
            <div class="main-list">
                <h2 class="title">Kết quả tìm thấy <span class="text-success">507</span> khách sạn ở <span class="text-success">Ho Chi Minh</span></h2>
                <!-- sort -->
                <div class="sort hidden-xs">
                    <span class="pull-left">Sắp xếp theo</span>
                    <div class="pull-right">
                        <div class="input-group">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Giá phòng">
                                <span class="input-group-addon"><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Hạng sao">
                                <span class="input-group-addon"><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                        <div class="input-group hidden-xs">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Tên A-Z">
                                <span class="input-group-addon"><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                        <div class="input-group  hidden-xs">
                            <div class="input-group">
                                <input type="text" class="form-control" placeholder="Đánh giá">
                                <span class="input-group-addon"><i class="fa fa-sort-down"></i></span>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- sort end -->

                <div class="hotel-list">

                    <div class="item best-price">
                        <div class="item-title">HOTDEAL GIÁ ƯU ĐÃI</div>
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star"><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i></div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="item">
                        <div class="col-md-4 featured-image hidden-sm">
                            <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                        </div>

                        <div class="col-md-8 hotel-info">
                            <div class="row">
                                <div class="col-sm-8 left hxs-7">
                                    <h3 class="pull-left"><a href="#">Liberty Central Saigon Citypoint</a></h3>
                                    <div class="clearfix"></div>
                                    <div class="star">
                                        <i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>
                                        <div class="pull-right facility">
                                            <i class="fa fa-car"></i>
                                            <i class="fa fa-glass"></i>
                                            <i class="fa fa-gavel"></i>
                                        </div>
                                    </div>
                                    <ul class="list-unstyled">
                                        <li class="address"><i class="fa fa-map-marker"></i> Quận 1, Tp. Hồ Chí Minh</li>
                                        <li class="viewing">22 người đang xem</li>
                                        <li class="bed-info"><i class="fa fa-user"></i>+<i class="fa fa-user"></i> 2 giường đơn</li>
                                        <li><strong>Chúng tôi còn 02 phòng</strong></li>
                                    </ul>
                                </div>
                                <div class="col-sm-4 right hxs-5">
                                    <ul class="list-unstyled text-center">
                                        <li class="mark"><strong>Rất tốt 8.2</strong></li>
                                        <li class="base-on">Dựa trên 2150 đánh giá</li>
                                        <li class="devider"></li>
                                        <li class="price"><span class="currency">VND</span> 5,135,000</li>
                                        <li class="price-for">Giá cho 3 đêm</li>
                                        <li><a href="#" class="btn btn-block">Đặt ngay</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>




                    <!-- Pagination bar -->
                    <div class="pagination-bar">

                        <div class="pull-left">Hiển thị</div>

                        <ul class="pagination pull-left">
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>
                            <li><span>5</span></li>
                            <li><span>>></span></li>
                        </ul>

                        <div class="back-to-top">Lên đầu trang</div>
                    </div>
                    <!-- Pagination bar -->



                </div>
            </div>
        </div>
        <!-- Main list end -->

        <!-- Sidebar Start -->
        <div class="col-md-3 col-md-pull-9">
            <div class="sidebar">
               

                <!-- Filter Start -->
                <div class="filter-box hidden-xs">
                    <div class="title">Chọn lọc theo</div>
                    <div class="filter-head">Giá 1 đêm/VND</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Từ 0 - 1,500,000</li>
                        <li><input type="checkbox"> Từ 1,500,000 - 3,500,000</li>
                        <li><input type="checkbox"> Từ 3,500,000 - 5,500,000</li>
                        <li><input type="checkbox"> Từ 5,500,000 - 7,500,000</li>
                        <li><input type="checkbox"> > 7,500.000</li>
                    </ul>
                    <div class="filter-head">Xếp hạng theo sao</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Từ 0 - 1,500,000</li>
                        <li><input type="checkbox"> Từ 1,500,000 - 3,500,000</li>
                        <li><input type="checkbox"> Từ 3,500,000 - 5,500,000</li>
                        <li><input type="checkbox"> Từ 5,500,000 - 7,500,000</li>
                        <li><input type="checkbox"> > 7,500.000</li>
                    </ul>
                    <div class="filter-head">Bữa ăn</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Bao gồm bữa ăn sáng</li>
                        <li><input type="checkbox"> Bữa ăn sáng & tối</li>
                        <li><input type="checkbox"> Bữa ăn trọn gói</li>
                    </ul>
                    <div class="filter-head">Loại chỗ ở</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Khách sạn</li>
                        <li><input type="checkbox"> Nhà trọ</li>
                        <li><input type="checkbox"> Căn hộ</li>
                        <li><input type="checkbox"> Nhà khách</li>
                        <li><input type="checkbox"> Chỗ nghỉ nhà dân</li>
                        <li><input type="checkbox"> Biệt thự</li>
                        <li><input type="checkbox"> Nhà nghỉ B&B</li>
                        <li><input type="checkbox"> Nhà nghỉ ven đường</li>
                        <li><input type="checkbox"> Resort</li>
                    </ul>
                    <div class="filter-head">Tiện nghi</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Wifi</li>
                        <li><input type="checkbox"> Chỗ đậu xe</li>
                        <li><input type="checkbox"> Xe đưa đón sân bay</li>
                        <li><input type="checkbox"> Trung tâm thể dục</li>
                        <li><input type="checkbox"> Phòng hút thuốc</li>
                        <li><input type="checkbox"> Hồ bơi trong nhà</li>
                        <li><input type="checkbox"> Trung tâm Spa & Sức khỏe</li>
                        <li><input type="checkbox"> Phòng gia đình</li>
                        <li><input type="checkbox"> Hồ bơi ngoài trời</li>
                        <li><input type="checkbox"> Cho phép mang vật nuôi</li>
                        <li><input type="checkbox"> Tiện nghi cho người khuyết tật</li>
                        <li><input type="checkbox"> Nhà hàng</li>
                    </ul>
                    <div class="filter-head">Quận</div>
                    <ul class="list-unstyled check-box-group">
                        <li><input type="checkbox"> Quận 1</li>
                        <li><input type="checkbox"> Quận 2</li>
                        <li><input type="checkbox"> Quận 3</li>
                        <li><input type="checkbox"> Quận 4</li>
                        <li><input type="checkbox"> Quận 5</li>
                        <li><input type="checkbox"> Quận 6</li>
                        <li><input type="checkbox"> Quận 7</li>
                        <li><input type="checkbox"> Quận 8</li>
                        <li><input type="checkbox"> Quận 9</li>
                        <li><input type="checkbox"> Quận 10</li>
                        <li><input type="checkbox"> Quận 11</li>
                        <li><input type="checkbox"> Quận 12</li>
                        <li><input type="checkbox"> Quận Bình Tân</li>
                        <li><input type="checkbox"> Quận Bình Thạnh</li>
                        <li><input type="checkbox"> Quận Thủ Đức</li>
                        <li><input type="checkbox"> Quận Tân Bình</li>
                        <li><input type="checkbox"> Huyện Củ Chi</li>
                    </ul>
                </div>
                <!-- Filter End -->

            </div>
        </div>
        <!-- Sidebar End -->

    </div>
</main>
<!-- Main End -->
<!-- Popular hotel -->
<div class="popular-hotel hidden-xs">
    <div class="container">
        <div class="inner">
            <h2>Khách sạn phổ biến tại Tp. Hồ Chí Minh</h2>
            <div class="hotel-items row">

                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>
                <div class="item col-md-3 col-sm-6">
                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                    <h3><a href="#">Grand hotel Saigon</a></h3>
                    <p>Rất tốt 8.2</p>
                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                </div>

            </div>
        </div>
    </div>
</div>
<!-- Popular hotel End-->


<?php include 'footer.php'; ?>