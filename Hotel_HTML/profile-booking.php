<?php include 'header.php'; ?>
<main class="container">
    <div class="profile-booking">
        <div class="row">
            <!-- Main content -->
            <div class="col-md-9 col-md-push-3">
                <div class="main-content">
                    <!-- List menu profile booking -->
                    <section class="hidden-xs">
                        <div class="list-menu">
                            <div class="name">
                                <div class="name1">TRANG THÔNG TIN</div>
                            </div>
                            <div class="name">
                                <div class="name2 active">DANH SÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name3">DANH SÁCH KHÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name4">THÔNG TIN THẺ TÍN DỤNG</div>
                            </div>
                        </div>
                    </section>
                    <!-- End list menu profile booking -->

                    <section class="hidden-xs">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Số tham chiếu:</label>
                                <input type="text" name="" class="form-control form-profile" />
                                <label>Mã dự phòng:</label>
                                <input type="number" class="form-control form-profile" name="" />
                                <label>Họ:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <label>Tên đệm:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <label>Khách sạn:</label>
                                <input type="text" class="form-control form-profile" name="" />
                            </div>
                            <div class="col-md-6">
                                <label>Tình trạng:</label>
                                <input type="text" name="" class="form-control form-profile" />
                                <label>Ngày nhận phòng:</label>
                                <input type="date" class="form-control form-profile" name="" />
                                <label>Ngày trả phòng:</label>
                                <input type="date" class="form-control form-profile" name="" />
                                <label>Giá phòng:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <button class="pull-right btn btn-danger">TÌM BOOKING</button>
                            </div>
                        </div>
                    </section>
                    
                    <!-- List room hotel -->
                    <section>
                        <table class="responsive">
                            <tr>
                                <th>MÃ THAM CHIẾU</th>
                                <th>MÃ ĐẶT PHÒNG</th>
                                <th>TÌNH TRẠNG</th>
                                <th>NHẬN PHÒNG</th>
                                <th>TRẢ PHÒNG</th>
                                <th>KHÁCH SẠN</th>
                                <th>TỔNG GIÁ TRỊ</th>
                                <th>HỌ</th>
                                <th>TÊN ĐỆM</th>
                                <th>THANH TOÁN</th>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                            <tr style="text-align: center">
                                <td>BC1015-AA01376</td>
                                <td>XI-HF-28475912</td>
                                <td>Booked</td>
                                <td>15/11/2015</td>
                                <td>17/11/2015</td>
                                <td>Rex Saigon</td>
                                <td>$4,082,000</td>
                                <td>Dao</td>
                                <td>Huy Thinh</td>
                                <td><span><i class="fa fa-money"></i> </span> <span> <i class="fa fa-cog"></i> </span> <span> <i class="fa fa-times"></i></span></td>
                            </tr>
                        </table>
                    </section>
                    
                    <!-- End list room hotel -->
                </div>
            </div>
            <!-- End main content -->

            <?php require 'profile-sidebar.php';?>
        </div>
    </div>
</main>
<?php include 'footer.php'; ?>