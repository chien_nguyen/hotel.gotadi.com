<?php require_once 'header.php'; ?>
<main class="container">
    <div class="profile-booking">
        <div class="row">
            <!-- Main content -->
            <div class="col-md-9 col-md-push-3">
                <div class="main-content">
                    <!-- List menu profile booking -->
                    <section>
                        <div class="list-menu">
                            <div class="name">
                                <div class="name1">TRANG THÔNG TIN</div>
                            </div>
                            <div class="name">
                                <div class="name2">DANH SÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name3 active">DANH SÁCH KHÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name4">THÔNG TIN THẺ TÍN DỤNG</div>
                            </div>
                        </div>
                    </section>
                    <!-- End list menu profile booking -->

                    <section class="hidden-xs">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Tên đệm & Tên:</label>
                                <input type="text" name="" class="form-control form-profile" />
                                <label>Họ:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <label>Họ:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <label>Địa chỉ:</label>
                                <input type="text" class="form-control form-profile" name="" />
                            </div>
                            <div class="col-md-6">
                                <label>Giới tính:</label>
                                <select class="form-control form-profile">
                                    <option>Nam</option>
                                    <option>Nữ</option>
                                    <option>Bê đê</option>
                                </select>
                                <label>Ngày sinh:</label>
                                <input type="date" class="form-control form-profile" name="" />
                                <label>Điện thoại di động:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <label>Điện thoại bàn:</label>
                                <input type="text" class="form-control form-profile" name="" />
                                <button class="pull-right btn btn-danger">THÊM KHÁCH</button>
                            </div>
                        </div>
                    </section>

                    <!-- List room hotel -->
                    <section>
                        <table class="responsive">
                            <tr>
                                <th >HỌ</th>
                                <th >TÊN ĐỆM & TÊN</th>
                                <th >NGÀY SINH</th>
                                <th>ĐỊA CHỈ</th>
                                <th>SỐ DI ĐỘNG</th>
                                <th>ĐIỆN THOẠI BÀN</th>
                            </tr>
                            <tr style="text-align: center">
                                <td>DAO</td>
                                <td>HUY THINH</td>
                                <td>12/04/1984</td>
                                <td>194 Nguyễn Đình Chiểu, P6, Q3, TPHCM</td>
                                <td>0123456789</td>
                                <td>083859687</td>
                            </tr>
                            <tr style="text-align: center">
                                <td>DAO</td>
                                <td>HUY THINH</td>
                                <td>12/04/1984</td>
                                <td>194 Nguyễn Đình Chiểu, P6, Q3, TPHCM</td>
                                <td>0123456789</td>
                                <td>083859687</td>
                            </tr>
                            <tr style="text-align: center">
                                <td>DAO</td>
                                <td>HUY THINH</td>
                                <td>12/04/1984</td>
                                <td>194 Nguyễn Đình Chiểu, P6, Q3, TPHCM</td>
                                <td>0123456789</td>
                                <td>083859687</td>
                            </tr>
                            <tr style="text-align: center">
                                <td>DAO</td>
                                <td>HUY THINH</td>
                                <td>12/04/1984</td>
                                <td>194 Nguyễn Đình Chiểu, P6, Q3, TPHCM</td>
                                <td>0123456789</td>
                                <td>083859687</td>
                            </tr>
                            <tr style="text-align: center">
                                <td>DAO</td>
                                <td>HUY THINH</td>
                                <td>12/04/1984</td>
                                <td>194 Nguyễn Đình Chiểu, P6, Q3, TPHCM</td>
                                <td>0123456789</td>
                                <td>083859687</td>
                            </tr>
                        </table>
                    </section>

                    <!-- End list room hotel -->
                </div>
            </div>
            <!-- End main content -->

            <?php require 'profile-sidebar.php';?>
        </div>
    </div>
</main>
<?php include 'footer.php'; ?>