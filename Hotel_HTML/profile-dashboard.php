<?php include 'header.php'; ?>
<main class="container">
    <div class="profile-booking">
        <div class="row">
            <!-- Main content -->
            <div class="col-md-9 col-md-push-3">
                <div class="main-content">
                    <!-- List menu profile booking -->
                    <section class="hidden-xs">
                        <div class="list-menu">
                            <div class="name">
                                <div class="name1 active">TRANG THÔNG TIN</div>
                            </div>
                            <div class="name">
                                <div class="name2">DANH SÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name3">DANH SÁCH KHÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name4">THÔNG TIN THẺ TÍN DỤNG</div>
                            </div>
                        </div>
                    </section>
                    <!-- Main profile -->
                    <section>
                        <div class="row">
                            <div class="col-md-8">
                                <section>
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="group-profile">
                                                <div class="title">LỊCH SỬ HOẠT ĐỘNG</div>
                                                <div class="group-name">
                                                    <div class="list-name">Lần dăng nhập cuối:</div>
                                                    <div class="pull-right">14/10/2015</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Số lần đã đăng nhập:</div>
                                                    <div class="pull-right">17</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Số phòng đã thanh toán:</div>
                                                    <div class="pull-right">14</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Số phòng đang giữ:</div>
                                                    <div class="pull-right">12</div>
                                                </div>
                                                <div class="group-name"> </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <div class="group-profile">
                                                <div class="title-2">THỐNG KÊ GIAO DỊCH</div>
                                                <div class="group-name">
                                                    <div class="list-name">Đơn vị tiền tệ:</div>
                                                    <div class="pull-right">VND</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Số tiền trong tài khoản:</div>
                                                    <div class="pull-right">12,000,000</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Số tiền đã thanh toán:</div>
                                                    <div class="pull-right">52,800.000</div>
                                                </div>
                                                <div class="group-name">
                                                    <div class="list-name">Tổng giá phòng đang giữ:</div>
                                                    <div class="pull-right">5,500,000</div>
                                                </div>
                                                <div class="group-name"> </div>
                                            </div>
                                        </div>
                                    </div>
                                </section>
                                <section>
                                    <div class="favorite">
                                        <div class="name-favorite">DANH SÁCH YÊU THÍCH CỦA BẠN</div>
                                        <!-- Popular hotel -->
                                        <div class="popular-hotel-2">
                                            <div class="hotel-items">
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                                <div class="item">
                                                    <img src="img/hotel.jpg" class="img-responsive" alt=""/>
                                                    <h3><a href="#">Grand hotel Saigon</a></h3>
                                                    <p>Rất tốt 8.2</p>
                                                    <p>Từ <span>VND</span> <span class="price">1,450,000</span></p>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- Popular hotel End-->
                                    </div>
                                </section>

                                <!-- Comment -->
                                <section>
                                    <div class="comment">
                                        <div class="name-comment">ĐÁNH GIÁ CỦA BẠN</div>
                                        <p><strong>[ Rex Hotel ] </strong> Khách sạn rất tốt, nhân viên thân thiện, tiện nghi đầy đủ.</p>
                                        <p><strong>[ Rex Hotel ] </strong> Sang trọng, tiện nghi, chuyên nghiệp là những gì để mô tả khách sạn này.</p>
                                        <p><strong>[ Rex Hotel ] </strong> Dù không trong khu vực trung tâm nhưng bù lại khách sạn có view đẹp, yên tĩnh, thoáng mát</p>
                                        <p><strong>[ Rex Hotel ] </strong> Khách sạn rất tốt, nhân viên thân thiện, tiện nghi đầy đủ.</p>
                                        <p><strong>[ Rex Hotel ] </strong> Sang trọng, tiện nghi, chuyên nghiệp là những gì để mô tả khách sạn này.</p>
                                        <div class="pull-right"><a href="#">Xem thêm >></a></div>
                                    </div>
                                </section>
                                <!-- End comment -->
                            </div>
                            <div class="col-md-4">
                                <div class="right-sidebar">
                                    <div class="viewed"> BẠN ĐÃ XEM</div>
                                    <?php
                                    for ($i = 1; $i <= 8; $i++) {
                                        echo ' <div class="list-viewed">
                                        <div class="left-view">
                                            <div style="width: 85px; max-height: 90px; overflow: hidden;"><img src="img/hotel/69803_165_z.jpg" alt="" class="img-responsive"/></div>
                                            <div class="view-price">Xem giá</div>
                                        </div>
                                        <div class="right-view">
                                            <div class="item">
                                                <h4><a href="#">Grand hotel Saigon</a></h4>
                                                <div>Rất tốt 8.2</div>
                                                <div>Từ <span>VND</span> <span class="price">1,450,000</span></div>
                                            </div>
                                        </div>
                                    </div>';
                                    }
                                    ?>

                                </div>
                            </div>
                        </div>
                    </section>
                    <!-- End main profile -->
                </div>
            </div>
            <!-- End main content -->

            <?php require 'profile-sidebar.php';?>
        </div>
    </div>
</main>
<?php include 'footer.php'; ?>