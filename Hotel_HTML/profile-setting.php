<?php include 'header.php'; ?>
<main class="container">
    <div class="profile-booking profile-setting">
        <div class="row">
            <!-- Main content -->
            <div class="col-md-9 col-md-push-3">
                <div class="main-content">
                    <!-- List menu profile booking -->
                    <section>
                        <div class="list-menu">
                            <div class="name">
                                <div class="name1 active">TRANG THÔNG TIN</div>
                            </div>
                            <div class="name">
                                <div class="name2">DANH SÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name3">DANH SÁCH KHÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name4">THÔNG TIN THẺ TÍN DỤNG</div>
                            </div>
                        </div>
                    </section>
                    <!-- End list menu profile booking -->

                    <!-- Form thông tin -->
                    <section class="about">
                        <div class="l-about">
                            <div class="name">THÔNG TIN VỀ BẠN</div>
                            <div class="row">
                                <div class="col-sm-4">
                                    <div class="avatar">
                                        <div class="img-avatar">
                                            <img src="img/hotel/69803_165_z.jpg" class="img-responsive img-circle" alt="avatar"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="select-file">
                                        <input type="file" class="form-control"/>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                
                                
                                
                                <form class="form-bordered">
                                
                                
                                <div class="col-sm-6">
                                    
                                    <div class="form-group">
                                        <label class="control-label">Tên hiển thị: </label>
                                            <input type="text" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ngày sinh: </label>
                                        <input type="date" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Thành phố: </label>
                                        <select class="form-control">
                                            <option>HCM</option>
                                            <option>BMT</option>
                                            <option>Da Nang</option>
                                            <option>HN</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Quốc gia: </label>
                                        <select class="form-control">
                                            <option>Lào</option>
                                            <option>Cambodia</option>
                                            <option>Việt Nam</option>
                                            <option>Trung Quốc</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Zip code: </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Điện thoại: </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div style="margin-bottom: 15px">THÔNG TIN GIẤY TỜ:</div>
                                    <div class="form-group">
                                        <label class="control-label">Loại giấy tờ: </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Số CMND/ Hộ chiếu: </label>
                                        <input type="date" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Ngày hết hạn: </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label">Quốc gia cấp: </label>
                                        <input type="text" class="form-control"/>
                                    </div>
                                </div>
                                
                                </form>
                                
                                
                            </div>
                            <div class="form-group">
                                <button class="btn btn-primary">LƯU THAY ĐỔI</button>
                                <button class="btn btn-default">XÓA THAY ĐỔI</button>
                            </div>
                        </div>
                    </section>
                    <section class="about">
                        <div class="l-about">
                            <div class="name">KẾT NỐI FACEBOOK</div>
                            <div class="row">
                                <div style="padding: 25px 15px;overflow: hidden">
                                    <div class="col-md-4">
                                        <div style="">Email / Tên tài khoản:</div>
                                        <div style="">Mật khẩu:</div>
                                    </div>
                                    <div class="col-md-8">
                                        <div style="font-style: italic;color: #666666;font-size: 10pt;"><i class="fa fa-info-circle"></i> Tài khoản của bạn sẽ được kết nối với Facebook. Chúng tôi sẽ không bao giờ đăng lên trang Facebook của bạn khi chưa được bạn đồng ý.</div>
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <button class="btn btn-primary">LƯU THAY ĐỔI</button>
                                <button class="btn btn-default">XÓA THAY ĐỔI</button>
                            </div>
                        </div>
                    </section>
                    <section class="about">
                        <div class="l-about">
                            <div class="name">THAY ĐỔI MẬT KHẨU</div>
                            <div class="row">
                                <div style="padding-top: 20px" class="col-md-7">
                                    <div class="group">
                                        <div class="control-label">Mật khẩu cũ: </div>
                                        <input type="password" class="pull-right form-control" style="max-width: 250px"/>
                                    </div>
                                    <div class="group">
                                        <div class="control-label">Mật khẩu mới: </div>
                                        <input type="password" class="pull-right form-control" style="max-width: 250px"/>
                                    </div>
                                    <div class="group">
                                        <div class="control-label">Xác nhận mật khẩu mới: </div>
                                        <input type="password" class="pull-right form-control" style="max-width: 250px"/>
                                    </div>
                                </div>
                            </div>
                            <div class="group">
                                <button class="btn btn-primary">LƯU THAY ĐỔI</button>
                                <button class="btn btn-default">XÓA THAY ĐỔI</button>
                            </div>
                        </div>
                    </section>
<!--                    <section class="about">
                        <div class="l-about">
                            <div class="name">ĐĂNG KÝ NHẬN BẢN TIN</div>
                            <p style="padding: 15px;">Hãy đăng ký nhận những bản tin để luôn nhận được thông báo mới về các thông tin, chương trình giảm giá ưu đãi đặc biệt.</p>
                            <p style="padding-left: 15px">Email nhận bản tin: <strong>tathanhthuy220488@gmail.com</strong> <button class="btn btn-danger">Thay đổi</button></p>
                            <div class="group">
                                <button class="btn btn-primary">LƯU THAY ĐỔI</button>
                                <button class="btn btn-default">XÓA THAY ĐỔI</button>
                            </div>
                        </div>
                    </section>-->
                    <!-- End Form thông tin -->
                </div>
            </div>
            <!-- End main content -->

            <?php require 'profile-sidebar.php';?>
        </div>
    </div>
</main>
<?php include 'footer.php'; ?>