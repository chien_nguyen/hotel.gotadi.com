<!-- Sidebar -->
<div class="col-md-3 col-md-pull-9">
    <div class="sidebar">
        <section>
            <div class="name">
                <div style="font-size: 18pt; font-weight: bold;">TẠ THANH THÚI</div>
                <div style="font-size: 10pt">Trở thành thành viên từ 2015</div>
            </div>
            <div class="avatar">
                <img src="img/hotel/69803_165_z.jpg" class="img-responsive" alt="avatar ú ù"/>
            </div>
            <div class="list-book">
                DANH SÁCH ĐẶT PHÒNG
            </div>
        </section>

        <ul class="list-info">
            <li class="list-info1">
                <a href="#"> Thông tin của bạn </a>
            </li>
            <li class="list-info2">
                <a href="#"> Kết nối Facebook</a>
            </li>
            <li class="list-info3">
                <a href="#"> Lịch sử hoạt động</a>
            </li>
            <li class="list-info4">
                <a href="#"> Thống kê giao dịch</a>
            </li>
            <li class="list-info5">
                <a href="#"> Đổi mật khẩu</a>
            </li>
        </ul>

        <section>
            <div class="s-bay pull-left">SĂN VÉ MÁY BAY</div>
            <div class="s-hotel pull-right">TÌM KHÁCH SẠN</div>
        </section>
    </div>
</div>
<!-- End sidebar -->