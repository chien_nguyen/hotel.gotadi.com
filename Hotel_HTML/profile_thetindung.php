<?php include 'header.php'; ?>
<main class="container">
    <div class="profile-booking">
        <div class="row">
            <!-- Main content -->
            <div class="col-md-9 col-md-push-3">
                <div class="main-content">
                    <!-- List menu profile booking -->
                    <section>
                        <div class="list-menu">
                            <div class="name">
                                <div class="name1">TRANG THÔNG TIN</div>
                            </div>
                            <div class="name">
                                <div class="name2">DANH SÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name3">DANH SÁCH KHÁCH ĐẶT PHÒNG</div>
                            </div>
                            <div class="name">
                                <div class="name4 active">THÔNG TIN THẺ TÍN DỤNG</div>
                            </div>
                        </div>
                    </section>
                    <!-- Main profile -->
                    <div class="">THÔNG TIN THẺ TÍN DỤNG</div>
                    <!-- End main profile -->
                </div>
            </div>
            <!-- End main content -->

            <?php require 'profile-sidebar.php';?>
        </div>
    </div>
</main>
<?php include 'footer.php'; ?>