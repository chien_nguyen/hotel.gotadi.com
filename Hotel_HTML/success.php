<?php include 'header.php'; ?>
<main class="container content-step2">
        <section class="fail hidden-xs">
            <div class="step text-center">
                <div class="col-md-4">
                    <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                    <p>Chọn khách sạn</p>
                </div>
                <div class="col-md-4">
                    <p style="font-size: 30px; color: #044079;"><i style="background: #fff" class="fa fa-check-circle"></i></p>
                    <p style="max-width: 140px; display: inline-block;">Khách sạn của bạn & Thanh toán</p>
                </div>
                <div class="col-md-4">
                    <p style="font-size: 30px; color: #fff; display: inline; background: #044079; border-radius: 50%; padding: 1px 9px;">3</p><br>
                    <p style="max-width: 140px; display: inline-block;"> Đặt khách sạn thành công</p>
                </div>
            </div>
        </section>
    
        <section class="fail">
            <div class="content-success">
                <div class="success">
                    <div class="fail2">
                        <div class="notice">
                            <img src="img/icon/booking-success.png" alt="booking fail icon"/> &nbsp;  ĐẶT PHÒNG THÀNH CÔNG
                        </div>
                        <div class="inner">
                            <p>Xin chân thành cảm ơn quý khách Nguyen Van An! Đặt phòng của bạn đã được thanh toán thành công. Vui lòng kiểm tra lại thông tin đặt phòng của bạn:</p>
                        </div>
                        
                 <div class="suggest">
                            <div class="col-md-3 col-sm-3">
                                <div class="hotel">
                                    <div class="name-hotel">REX HOTEL <i class="fa fa-star-o"></i></div>
                                    <div class="img-hotel hidden-xs">
                                        <img class="img-responsive" src="img/hotel/69803_165_z.jpg" alt=""/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-6">
                                <div class="information-inner">
                                    <div class="date-label">
                                        <div>
                                            09 <br />
                                            10/2015
                                        </div>
                                        <div>Ngày nhận phòng</div>
                                    </div>
                                    <ul>
                                        <li><span>Mã tham chiếu: </span> ABC-123456</li> 
                                        <li><span>Ngày nhận phòng: </span> 07/10/2015</li> 
                                        <li><span>Khách hàng: </span> TRAN VAN B</li> 
                                        <li><span>Khu vực: </span> Quận 1</li> 
                                        <li><span>Bữa ăn: </span> </li> 
                                        <li><span>Dịch vụ thêm: </span> </li> 
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-6">
                                <div class="information-inner">
                                <div class="date-label">
                                    <div>
                                        09 <br />
                                        10/2015
                                    </div>
                                    <div>Ngày trả phòng</div>
                                </div>
                                    
                                    <p><strong>Điều kiện hủy:</strong></p>
                                    <p>2 ngày trước khi nhận phòng (10 tháng 10, 2015).</p>
                                    <p>Hủy miễn phí trước ngày 10 tháng 10, 2015 - 12:00.</p>
                                    <p>Hủy sau ngày 10 tháng 10, 2015 - 12:00 sẽ bị thu phí 1 đêm tiền phòng</p>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="inner">
                            <p>Bạn sẽ nhận được email thông báo của chúng tôi trong vài phút. Vui lòng kiểm tra trong hộp thư của bạn (hoặc trong mục Spam). </p>
                            <p>Để biết thêm thông tin hoặc nếu có thắc mắc cần chúng tôi giải quyết, xin quý khách đừng ngần ngại liên hệ với chúng tôi:</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    
        <section class="fail">
            <div class="book-hotel">
                <div class="name-book">GỢI Ý ĐẶT VÉ MÁY BAY ĐI ĐÀ NẴNG</div>
                <div class="form-book">
                    <div class="col-md-3">
                        <label>Bay từ: </label>
                        <select class="form-control form-group">
                            <option>Hà nợi</option>
                            <option>Hồ Chí Minh</option>
                            <option>Đà Nẵng</option>
                            <option>Buôn Ma Thuột</option>
                            <option>Cần Thơ</option>
                        </select>
                        <div class="search-advanced">
                            Tìm kiếm nâng cao
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label>Nơi đến</label>
                        <select class="form-control form-group">
                            <option>Hà nợi</option>
                            <option>Hồ Chí Minh</option>
                            <option>Đà Nẵng</option>
                            <option>Buôn Ma Thuột</option>
                            <option>Cần Thơ</option>
                        </select>
                        
                        <label>Người lớn</label>
                        <select class="form-control form-group">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Ngày đi</label>
                        <input class="form-control form-group" type="date" />
                        
                        <label>Trẻ em</label>
                        <select class="form-control form-group">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="col-md-3">
                        <label>Ngày về</label>
                        <input class="form-control form-group" type="date" />
                        <button class="btn btn-primary form-control">TÌM KIẾM NGAY</button>
                    </div>
                </div>
            </div>
        </section>
    </main>


<?php include 'footer.php'; ?>