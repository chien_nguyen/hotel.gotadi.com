﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Helpers;

using System.Threading.Tasks;

namespace ExpediaModels
{
    public class Hotels
    {
        public int Total { get; set; }
        public int Size { get; set; }
        public string SessionID { get; set; }
        public List<HotelDetailDO> HotelList { get; set; }

        public Hotels()
        { }

        public Hotels(string data)
        {
            var jsonObject = Json.Decode(data);

            Total = int.Parse(jsonObject.HotelListResponse.numberOfRoomsRequested.ToString());
            SessionID = jsonObject.HotelListResponse.customerSessionId;

            // Hotel list
            var arrData = (DynamicJsonArray)jsonObject.HotelListResponse.HotelList.HotelSummary;
            HotelList = arrData.Select(t => new HotelDetailDO(t, false)).ToList();
        }
    }

    public class HotelDetailDO
    {
        public int ubsScore { get; set; }
        public string HotelID { get; set; }
        public string HotelName { get; set; }

        public string HotelURL { get; set; }

        public string HotelAvatar { get; set; }
        public string StarRating { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string CountryCode { get; set; }
        public string ConfidenceRating { get; set; }
        public string TripAdvisorReviewCount { get; set; }
        public string TripAdvisorRatingUrl { get; set; }

        public string latitude { get; set; }
        public string longitude { get; set; }
        public string numberOfRooms { get; set; }
        public string numberOfFloors { get; set; }
        public string checkInTime { get; set; }
        public string checkOutTime { get; set; }
        public string propertyInformation { get; set; }
        public string areaInformation { get; set; }
        public string propertyDescription { get; set; }
        public string hotelPolicy { get; set; }
        public string roomInformation { get; set; }
        public string checkInInstructions { get; set; }
        public string knowBeforeYouGoDescription { get; set; }
        public string locationDescription { get; set; }
        public string amenitiesDescription { get; set; }
        public string businessAmenitiesDescription { get; set; }
        public string roomDetailDescription { get; set; }
        public string drivingDirections { get; set; }
        public string roomFeesDescription { get; set; }
        public string diningDescription { get; set; }
        public string lowRate { get; set; }


        public RoomDetailDO DefaultRoom { get; set; }
        public List<RoomDetailDO> AvailableRooms { get; set; }

        public List<HotelImage> HotelImages { get; set; }

        public List<AmenityDO> PropertyAmenities { get; set; }

        public HotelDetailDO() { }

        public HotelDetailDO(dynamic data, bool isDetail = false)
        {
            if (isDetail)
            {
                /////////////////////////////////////////////////////
                // Hotel detail with rate/room
                /////////////////////////////////////////////////////
                LoadAvailableRoomsRates(data);
                HotelID = data.HotelRoomAvailabilityResponse.hotelId.ToString();
                Address = data.HotelRoomAvailabilityResponse.hotelAddress;
                HotelName = data.HotelRoomAvailabilityResponse.hotelName;

                CountryCode = data.HotelRoomAvailabilityResponse.hotelCountry;
                City = data.HotelRoomAvailabilityResponse.hotelCity;

                // Hotel Image
                var arrHotelImages = (DynamicJsonArray)data.HotelRoomAvailabilityResponse.HotelImages.HotelImage;
                HotelImages = arrHotelImages.Select(t => new HotelImage(t)).ToList();

                HotelAvatar = HotelImages[0].Url.ToString();
                //HotelAvatar = path.Replace("t.jpg", "l.jpg");

                // Description
                propertyDescription = data.HotelRoomAvailabilityResponse.HotelDetails.propertyDescription;
                TripAdvisorReviewCount = data.HotelRoomAvailabilityResponse.tripAdvisorReviewCount.ToString();

                TripAdvisorRatingUrl = data.HotelRoomAvailabilityResponse.tripAdvisorRatingUrl;

                // Amenities
                var arrAmenity = (DynamicJsonArray)data.HotelRoomAvailabilityResponse.PropertyAmenities.PropertyAmenity;
                PropertyAmenities = arrAmenity.Select(t => new AmenityDO(t)).ToList();

                // Policies
                checkInTime = data.HotelRoomAvailabilityResponse.HotelDetails.checkInTime;
                checkOutTime = data.HotelRoomAvailabilityResponse.HotelDetails.checkOutTime;
                propertyInformation = data.HotelRoomAvailabilityResponse.HotelDetails.propertyInformation;
                areaInformation = data.HotelRoomAvailabilityResponse.HotelDetails.areaInformation;
                propertyDescription = data.HotelRoomAvailabilityResponse.HotelDetails.propertyDescription;
                hotelPolicy = data.HotelRoomAvailabilityResponse.HotelDetails.hotelPolicy;
                roomInformation = data.HotelRoomAvailabilityResponse.HotelDetails.roomInformation;
                drivingDirections = data.HotelRoomAvailabilityResponse.HotelDetails.drivingDirections;
                checkInInstructions = data.HotelRoomAvailabilityResponse.HotelDetails.checkInInstructions;
                knowBeforeYouGoDescription = data.HotelRoomAvailabilityResponse.HotelDetails.knowBeforeYouGoDescription;
                locationDescription = data.HotelRoomAvailabilityResponse.HotelDetails.locationDescription;
                roomFeesDescription = data.HotelRoomAvailabilityResponse.HotelDetails.roomFeesDescription;
                diningDescription = data.HotelRoomAvailabilityResponse.HotelDetails.diningDescription;
                amenitiesDescription = data.HotelRoomAvailabilityResponse.HotelDetails.amenitiesDescription;
                businessAmenitiesDescription = data.HotelRoomAvailabilityResponse.HotelDetails.businessAmenitiesDescription;
                roomDetailDescription = data.HotelRoomAvailabilityResponse.HotelDetails.roomDetailDescription;
            }
            else
            {
                /////////////////////////////////////////////////////
                // Search list
                /////////////////////////////////////////////////////
                HotelName = data.name;
                DefaultRoom = new RoomDetailDO(data.RoomRateDetailsList.RoomRateDetails, false);
                string path = "http://img-e.gotadi.com" + data.thumbNailUrl.ToString();
                HotelAvatar = path.Replace("t.jpg", "l.jpg");
                Address = data.address1;
                StarRating = (string)data.hotelRating.ToString();

                ConfidenceRating = data.confidenceRating.ToString();

                CountryCode = data.countryCode;
                City = data.city;
                HotelID = data.hotelId.ToString();

                TripAdvisorReviewCount = data.tripAdvisorReviewCount.ToString();

                TripAdvisorRatingUrl = data.tripAdvisorRatingUrl;
            }
           
           
            
            
        }


        public void LoadAvailableRoomsRates(dynamic data)
        {
            var arrHotelRoomResponse = (DynamicJsonArray)data.HotelRoomAvailabilityResponse.HotelRoomResponse;
            AvailableRooms = arrHotelRoomResponse.Select(t => new RoomDetailDO(t)).ToList();
        }
    }
}
