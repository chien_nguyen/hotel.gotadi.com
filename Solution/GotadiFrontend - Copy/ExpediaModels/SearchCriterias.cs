﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpediaModels
{
    public class RoomSearchCriteriaDO
    {
        public int RoomIndex { get; set; }
        public int NumberOfAdults { get; set; }
        public int NumberOfChildren { get; set; }
        public string ChildAges { get; set; }

        public RoomSearchCriteriaDO(int roomIndex, int numberOfAdults, int numberOfChildren, string childAges = "")
        {
            RoomIndex = roomIndex;
            NumberOfAdults = numberOfChildren;
            NumberOfChildren = numberOfChildren;
            ChildAges = childAges;
        }
    }

    public class HotelSearchCriteriaDO
    {
        public string Destination { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime Checkout { get; set; }

        public List<RoomSearchCriteriaDO> Rooms { get; set; }

        public HotelSearchCriteriaDO(string destination, DateTime checkIn, DateTime checkOut, List<RoomSearchCriteriaDO> rooms)
        {
            Destination = destination;
            CheckIn = checkIn;
            Checkout = checkOut;
            Rooms = rooms;
        }
    }
}
