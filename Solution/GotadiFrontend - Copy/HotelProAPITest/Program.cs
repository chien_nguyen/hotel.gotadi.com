﻿using System;
using System.Runtime;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using SupplierAPI;

namespace HotelProAPITest {
    class Program {
        static void Main(string[] args) {
            var svc = new HotelPro();
            var res = svc.getResponse(svc.getBalance());
            try {
                var res1 = JsonConvert.DeserializeObject<SupplierAPI.HotelProService.getBalanceResponse>(res);
                Console.WriteLine(res1);
            } catch(Exception e) {
                var res1 = JsonConvert.DeserializeObject<List<string>>(res);
                Console.WriteLine(res1[0] + " - " + res1[1]);
            }
            //var balance = JsonConvert.DeserializeObject<SupplierAPI.HotelProService.getBalanceResponse>(res);
            //var rooms = HotelProElement.Instance.GetRoomGuest(2, 0, 1);
            //var filters = HotelProElement.Instance.GetFilter("", "", 0, HotelProElement.BoardType.None, 0);
            //var getAvai = svc.getResponse(svc.getAvailableHotel("H0WC", DateTime.Now.AddDays(90), DateTime.Now.AddDays(91), "USD", "VN", false, rooms, filters, null));
           
            //Console.WriteLine(getAvai);
            Console.ReadLine();
        }
    }
}
