﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class InputCustomerController : Controller
    {
        // GET: InputCustomer
        public ActionResult Index(string hotelcode, string room_type, string room_code, DateTime checkin, DateTime checkout)
        {
            // Load rate type detail
            var HotelDetail = new ExpediaBusiness.RoomBL().GetRoomRateDetail();

            // Load room images
            var roomImages = new ExpediaBusiness.ImageBL().GetRoomImages("");

            ViewBag.HotelDetail = HotelDetail;
            ViewBag.RoomImages = roomImages;

            ViewBag.CheckInDay = checkin.Day;
            ViewBag.CheckOutDay = checkout.Day;

            ViewBag.CheckInMonthYear = checkin.ToString("MM/yyyy");
            ViewBag.CheckOutMonthYear = checkout.ToString("MM/yyyy");

            return View("~/Views/CustomerInfor/InputCustomerViewer.cshtml");
        }
    }
}