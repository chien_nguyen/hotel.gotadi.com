﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Presentation.Helpers;

namespace Presentation {
    public class MvcApplication : System.Web.HttpApplication {
        protected void Application_Start() {
            AreaRegistration.RegisterAllAreas();
            GlobalFilters.Filters.Add(new ResourceActionFilter(), 0);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
        protected void Application_BeginRequest(object sender, EventArgs e) {
            // Do Not Allow URL to end in trailing slash
            string url = HttpContext.Current.Request.Url.AbsolutePath;
            if (string.IsNullOrEmpty(url)
                || url.Trim() == "/"
                || url.Trim() == "\\")
                return;

            string lastChar = url[url.Length - 1].ToString();
            if (lastChar == "/" || lastChar == "\\") {
                url = url.Substring(0, url.Length - 1) + HttpContext.Current.Request.Url.Query;
                Response.Clear();
                Response.Status = "301 Moved Permanently";
                Response.AddHeader("Location", url);
                Response.End();
            }

            // Redirect URL (301 & 302) or Rewrite
            
        }
        public class ResourceActionFilter : ActionFilterAttribute {
            public override void OnActionExecuting(ActionExecutingContext filterContext) {
                filterContext.Controller.ViewBag.cdns = Utilities.Instance.GetConfig("cdns");
            }
        }
    }
}