﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Web;

namespace Presentation.Helpers {
    public class Links {
        string _slash = "/";
        string _minus = "-";
        string _quest = "?";
        static Links _ins;
        public static Links Instance {
            get {
                if (_ins == null) _ins = new Links();
                return _ins;
            }
        }
        public string GetDomain() {
            return HttpContext.Current.Request.Url.Host;
        }
        public string GetBase() {
            return ("//" + HttpContext.Current.Request.Url.Host +
                   (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port)).ToLower();
        }
        public string GetBase(string slug) {
            try {
                if (slug.Contains(this.GetBase()))
                    return slug.ToLower();
                var url = (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port) + _slash + slug;
                url = url.Replace("//", _slash);
                url = "//" + HttpContext.Current.Request.Url.Host + url;
                return url.ToLower();
            } catch (NullReferenceException e) {
                Vision.Instance.ExceptionWriteLine(e);
                return GetBase();
            }
        }
        public string GetScheme(string slug) {
            try {
                if (slug.Length <= 0) return String.Empty;
                if (!slug.StartsWith("http") && !slug.StartsWith("//"))
                    return "//" + slug;
                return slug;
            } catch (NullReferenceException e) {
                Vision.Instance.ExceptionWriteLine(e);
                return String.Empty;
            }
        }
        public string GetHotelPlace(string countrySlug, string placeSlug, string placeCode) {
            if (countrySlug.Length == 0 || placeSlug.Length == 0 || placeCode.Length == 0)
                return GetBase();
            return GetBase("khach-san-" + countrySlug + _slash + placeSlug + _minus + placeCode);
        }
        public string GetHotelPlace(string placeCode) {
            try {
                var repo = new Repositories.Places();
                var data = repo.GetByCode(placeCode);
                if (data != null)
                    return GetBase("khach-san-" + Utilities.Instance.MakeSlug(data.Country) + _slash + Utilities.Instance.MakeSlug(data.City) + _minus + data.DestinationCode);
                else
                    return GetBase();
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return GetBase();
            }
        }
        public string GetHotelPlace(string placeCode, string querystring) {
            return GetHotelPlace(placeCode) + (querystring.Length > 0 ? _quest + querystring : "");
        }
        public string GetHotelDetail(string countrySlug, string placeSlug, string hotelSlug, string placeCode, string hotelCode) {
            if (countrySlug.Length == 0 || placeSlug.Length == 0 || hotelSlug.Length == 0 || placeCode.Length == 0 || hotelCode.Length == 0)
                return GetBase();
            return GetBase("khach-san-" + countrySlug + _slash + placeSlug + _minus + placeCode + _slash + hotelSlug + _minus + hotelCode);
        }
        public string GetHotelDetail(string countrySlug, string placeSlug, string hotelSlug, string placeCode, string hotelCode, string querystring) {
            return GetHotelDetail(countrySlug, placeSlug, hotelSlug, placeCode, hotelCode) + (querystring.Length > 0 ? _quest + querystring : "");
        }
        public string GetHotelDetail(string placeCode, Models.ks_Hotels hotelItem) {
            try {
                var data = (new Repositories.Places()).GetByCode(placeCode);
                if (data != null && hotelItem != null)
                    return GetHotelDetail(Utilities.Instance.MakeSlug(data.Country), Utilities.Instance.MakeSlug(data.City), Utilities.Instance.MakeSlug(hotelItem.HotelName), data.DestinationCode, hotelItem.HotelCode);
                else
                    return GetBase();
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return GetBase();
            }
        }
        public string GetHotelDetail(string placeCode, string hotelCode) {
            try {
                var data = (new Repositories.Hotels()).GetByCode(hotelCode);
                return GetHotelDetail(placeCode, data);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return GetBase();
            }
        }

        public static string GetHotelURL(string HotelCode, string HotelName, string City, string CountryCode)
        {
            RegionInfo myRI1 = new RegionInfo(CountryCode);
            //return GetBase("khach-san-" + countrySlug + _slash + placeSlug + _minus + placeCode);
            //var domain = (HttpContext.Current.Request.Url.IsDefaultPort ? "" : ":" + HttpContext.Current.Request.Url.Port);
            var domain = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            var s = string.Format("{0}/khach-san/{1}/khach-san-{2}/{3}-{4}",
                                domain,
                                Utilities.Instance.MakeSlug(myRI1.EnglishName),
                                Utilities.Instance.MakeSlug(City),
                                Utilities.Instance.MakeSlug(HotelName),
                                HotelCode);

            return s;
        }

        public static string GetBookRoomURL(string HotelCode, string RoomTypeCode, 
                                            string RateCode, string RateKey, ExpediaModels.HotelSearchCriteriaDO SearchCriterias)
        {
            var domain = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
            string strRooms = "";
            int idx = 1;
            foreach (var item in SearchCriterias.Rooms)
            {
                strRooms = strRooms + string.Format("&adult{0}={1}&child{0}={2}&childage{0}={3}",
                                        idx++, item.NumberOfAdults, item.NumberOfChildren, item.ChildAges
                                        );
            }

            var s = string.Format("{0}/thong-tin-khach/?hc={1}&rt={2}&rc={3}&rk={4}{5}&checkin={6}&checkout={7}",
                                domain,
                                HotelCode, RoomTypeCode, RateCode, RateKey,
                                strRooms,
                                SearchCriterias.CheckIn.ToString("yyyy-MM-dd"),
                                SearchCriterias.Checkout.ToString("yyyy-MM-dd")
                                );

            return s;
        }
    }
}
