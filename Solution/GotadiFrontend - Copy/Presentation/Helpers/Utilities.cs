﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Globalization;

namespace Presentation.Helpers {
    public class Utilities {
        static Utilities _ins;
        public static Utilities Instance {
            get {
                if (_ins == null) _ins = new Utilities();
                return _ins;
            }
        }
        public string ParseMetadataListToString(List<Models.Metadata> i) {
            List<string> o = new List<string>();
            i.ForEach(l => { if (!l.value.Equals("0") && l.value.Length > 0) { o.Add(l.key + "=" + l.value); } });
            return String.Join("&", o);
        }
        public string MakeSlug(string text) {
            text = RemoveUnicode(text);
            text = TrimAll(text);
            return ReplaceSpace(text);
        }
        public string TrimAll(string str) {
            if (string.IsNullOrWhiteSpace(str))
                return string.Empty;
            return System.Text.RegularExpressions.Regex.Replace(str.Trim(), @"\s+", " ");
        }
        public string RemoveUnicode(string text) {
            text = Regex.Replace(text, "[^\\w\\._ ]", "");

            string[] arr1 = new string[] { "á", "à", "ả", "ã", "ạ", "â", "ấ", "ầ", "ẩ", "ẫ", "ậ", "ă", "ắ", "ằ", "ẳ", "ẵ", "ặ",
                                            "đ",
                                            "é","è","ẻ","ẽ","ẹ","ê","ế","ề","ể","ễ","ệ",
                                            "í","ì","ỉ","ĩ","ị",
                                            "ó","ò","ỏ","õ","ọ","ô","ố","ồ","ổ","ỗ","ộ","ơ","ớ","ờ","ở","ỡ","ợ",
                                            "ú","ù","ủ","ũ","ụ","ư","ứ","ừ","ử","ữ","ự",
                                            "ý","ỳ","ỷ","ỹ","ỵ" };
            string[] arr2 = new string[] { "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a", "a",
                                            "d",
                                            "e","e","e","e","e","e","e","e","e","e","e",
                                            "i","i","i","i","i",
                                            "o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o","o",
                                            "u","u","u","u","u","u","u","u","u","u","u",
                                            "y","y","y","y","y" };
            for (int i = 0; i < arr1.Length; i++) {
                text = text.Replace(arr1[i], arr2[i]);
                text = text.Replace(arr1[i].ToUpper(), arr2[i].ToUpper());
            }
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex(@"[ ]{2,}", options);
            return regex.Replace(text, @" ");
        }
        public string ReplaceSpace(string text) {
            return text.Replace(" ", "-");
        }
        public string GetLastCode(string text) {
            try {
                if (!text.Contains("-"))
                    return String.Empty;
                string[] result = text.Split('-');
                return result.Last();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return String.Empty;
            }
        }
        public string GetConfig(string key) {
            try {
                return ConfigurationManager.AppSettings[key].ToString();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return String.Empty;
            }
        }
        public double ParseDouble(string i) {
            try {
                return Convert.ToDouble(i);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return 0;
            }
        }
        public int ParseInt(string i) {
            try {
                return Convert.ToInt32(i);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return 0;
            }
        }
        public int ParseInt(double i) {
            try {
                return Convert.ToInt32(i);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return 0;
            }
        }
        public double Ceiling(double number, int fractNumber) {
            if (fractNumber < 0) return 0;
            double floor = Math.Floor(number);
            double fract = number - floor;
            fract = Math.Ceiling(fract * Math.Pow(10, fractNumber));
            return number + fract / Math.Pow(10, fractNumber);
        }
        public string PriceFormat(double o) {
            if (o <= 0) return "";
            return String.Format(new CultureInfo("is-IS"), "{0:N0}", o);
        }
        public string PriceFormat(object o) {
            if (o == null)
                return String.Empty;
            string str = o.ToString();
            double temp;
            if (!Double.TryParse(str, out temp))
                return String.Empty;
            return String.Format(new CultureInfo("is-IS"), "{0:N0}", temp);
        }
        public string PriceFormat(double price, string currency, string language) {
            if (Utilities.Instance.ParseInt(price) <= 0)
                return "";
            string o = String.Format(new CultureInfo("is-IS"), "{0:N0}", price);
            if (language.ToLower() == "vi")
                o = o + " " + currency.ToUpper();
            else
                o = currency.ToUpper() + " " + o;
            return o;
        }
        public string PriceFormat(object price, string currency, string language) {
            if (price == null)
                return String.Empty;
            string str = price.ToString();
            double temp;
            if (!Double.TryParse(str, out temp))
                return String.Empty;
            return PriceFormat(temp, currency, language);
        }
        public int GetPaginationTakeItems() {
            int pageItems = Utilities.Instance.ParseInt(Utilities.Instance.GetConfig("pitm"));
            if (pageItems == 0) pageItems = 15; // DEFAULT VALUE
            return pageItems;
        }
        public int GetPaginationSiblings() {
            int pageSiblings = Utilities.Instance.ParseInt(Utilities.Instance.GetConfig("psib"));
            if (pageSiblings == 0) pageSiblings = 2; // DEFAULT VALUE
            return pageSiblings;
        }
        public int GetPaginationTotal(int totalItems) {
            int pageItems = GetPaginationTakeItems();
            return totalItems % pageItems == 0 ? totalItems / pageItems : totalItems / pageItems + 1;
        }
        public int GetPaginationSkipItems(int pageIndex) {
            if (pageIndex == 0) return 0;
            return GetPaginationTakeItems() * (pageIndex - 1);
        }
        public List<int> GetPaginationList(int pageIndex, int totalPages) {
            int pageSiblings = GetPaginationSiblings();
            var res = new List<int>();
            if (totalPages == 0) return res;
            if (pageIndex == 1) {
                for (var i = pageIndex; i <= (pageIndex + pageSiblings); i++) {
                    res.Add(i);
                    if (i == totalPages) break;
                }
                if (pageIndex + pageSiblings + 1 <= totalPages)
                    res.Add(totalPages);
            } else if (pageIndex == totalPages) {
                for (var i = pageIndex; i >= (pageIndex - pageSiblings); i--) {
                    res.Add(i);
                    if (i == 1) break;
                }
                if (pageIndex - pageSiblings - 1 >= 1)
                    res.Add(1);
            } else {
                if (pageIndex - pageSiblings <= 1)
                    for (var i = 1; i < pageIndex; i++)
                        res.Add(i);
                else {
                    for (var i = (pageIndex - pageSiblings); i < pageIndex; i++)
                        res.Add(i);
                    res.Add(1);
                }
                res.Add(pageIndex);
                if (pageIndex + pageSiblings >= totalPages)
                    for (var i = pageIndex + 1; i <= totalPages; i++)
                        res.Add(i);
                else {
                    for (var i = pageIndex + 1; i <= (pageIndex + pageSiblings); i++)
                        res.Add(i);
                    res.Add(totalPages);
                }
            }
            res.Sort((a, b) => a.CompareTo(b));
            return res;
        }

        public string GetReviewPoint(double point) {
            return String.Format(new CultureInfo("en-US"), "{0:N1}", point);
        }
        public string GetReviewMessage(double point) {
            string[] rate = new string[] {
                 "Chưa có",
                 "Bình thường",
                 "Tốt",
                 "Rất tốt",
                 "Tuyệt vời"
            };
            if (point <= 0)
                return rate[0];
            //point = point * 2;
            if (point < 6)
                return rate[1];
            if (point >= 6 && point < 7)
                return rate[2];
            if (point >= 7 && point < 9)
                return rate[3];
            if (point >= 9 && point <= 10)
                return rate[4];
            return rate[0];
        }
    }
}