﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers {
    public class Vision {
        static Vision _ins;
        public static Vision Instance {
            get {
                if (_ins == null) _ins = new Vision();
                return _ins;
            }
        }
        public enum RailwaySwitch { Success, Failure }
        public void ExceptionWriteLine(Exception e) {
            Console.WriteLine(e.Message);
        }
    }
}