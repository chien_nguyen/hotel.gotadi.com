﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models {
    public class HotelList {
        public Place index { get; set; }
        public List<Metadata> metadata { get; set; }
        public List<Breadcrumb> breadcrumb { get; set; }
        public List<Link> filterStarRate { get; set; }
        public Pagination page { get; set; }
        public List<HotelSlim> hotelItems { get; set; }
    }
    public class HotelDetail {
        public HotelFull index { get; set; }
        public List<Metadata> metadata { get; set; }
        public List<Breadcrumb> breadcrumb { get; set; }
        public List<string> slider { get; set; }
        public string hotelProSearchId { get; set; }
        public List<RoomDetail> rooms { get; set; }
        public List<HotelSlim> relatedItems { get; set; }
    }
    public class HotelSlim {
        public string hotelCode { get; set; }
        public string hotelLink { get; set; }
        public string hotelName { get; set; }
        public string hotelRate { get; set; }
        public string hotelImage { get; set; }
        public string hotelAddress { get; set; }
        public string hotelPromotion { get; set; }
        public string geoLatitude { get; set; }
        public string geoLongitude { get; set; }
        public string minPrice { get; set; }
        public string maxPrice { get; set; }
        public int reviewCount { get; set; }
        public string reviewPoint { get; set; }
        public string reviewMessage { get; set; }
        public string descriptionLite { get; set; }
        public List<SpriteBadge> badges { get; set; }

        //more - clear if have better solution
        public string currency { get; set; }
        public RoomSlim roomFirst { get; set; }
    }
    public class HotelFull : HotelSlim {
        public string descriptionFull { get; set; }
        public string checkInTime { get; set; }
        public string checkOutItem { get; set; }
        public string checkInGuide { get; set; }
        public List<string> amenitiesPrimary { get; set; }
        public List<string> amenitiesRoom { get; set; }
        public List<string> nearBy { get; set; }
    }
    public class RoomSlim {
        public string hotelProProcessId { get; set; }
        public string roomCategory { get; set; }
        public string roomBoardType { get; set; }
        public int roomItems { get; set; }
        public string minPrice { get; set; }
        public string maxPrice { get; set; }
        public string currency { get; set; }
    }
    public class RoomDetail: RoomSlim {
        public string roomPayLink { get; set; }
        public string roomImage { get; set; }
        public string roomBedType { get; set; }
        public string roomPromotion { get; set; }
        public string description { get; set; }
        public string cancellationPolicy { get; set; }
        public int incAdult { get; set; }
        public int incChild { get; set; }
        public int maxAdult { get; set; }
        public int maxChild { get; set; }
        public int maxTotal { get; set; }
        public List<SpriteBadge> badges { get; set; }
    }
    public class ReviewDetail {
        public string title { get; set; }
        public string fullname { get; set; }
        public string point { get; set; }
        public string message { get; set; }
        public string content { get; set; }
        public string datetime { get; set; }
    }
    public class Place {
        public string placeCode { get; set; }
        public string placeCountry { get; set; }
        public string placeCity { get; set; }
    }
}