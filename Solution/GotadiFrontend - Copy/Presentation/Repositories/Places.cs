﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.Models;

namespace Presentation.Repositories {
    public class Places {
        public Places() { }
        public ks_Destination GetByCode(string code) {
            try {
                using (DataContext ctx = new DataContext()) {
                    return ctx.ks_Destination.FirstOrDefault(l => l.DestinationCode == code);
                }
            } catch(Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}