﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using System.Web.Helpers;

using System.Threading.Tasks;
using System.Globalization;

namespace ExpediaModels
{
    public class AmenityDO
    {
        public string AmenityId { get; set; }
        public string Amenity { get; set; }

        public AmenityDO(dynamic data)
        {
            AmenityId = data["@amenityId"];
            //Amenity = data["@amenity"];
            Amenity = data.amenity;
        }
    }

    public class HotelImage
    {
        public string HotelImageId { get; set; }
        public string Name;
        public string Category;
        public string ImageType;
        public string Caption;
        public string Url;
        public string ThumbnailUrl;
        public string SupplierId;
        public string Width;
        public string Height;
        public string ByteSize;

        public HotelImage() { }
        public HotelImage(dynamic data)
        {
            HotelImageId = data.hotelImageId.ToString();
            Name = data.name;
            Category = data.category.ToString();
            ImageType = data.type.ToString();
            Caption = data.caption;
            Url = data.url.Replace("b.jpg", "z.jpg");
            ThumbnailUrl = data.thumbnailUrl;
            SupplierId = data.supplierId.ToString();
            Width = data.width.ToString();
            Height = data.height.ToString();
            ByteSize = data.byteSize.ToString();
        }
    }

    public class BedType
    {
        public string id;
        public string description;

        public BedType(dynamic data)
        {
            id = data["@id"];
            description = data.description;
        }
    }

    public class ValueAdd
    {
        public string id;
        public string description;

        public ValueAdd(dynamic data)
        {
            id = data["@id"];
            description = data.description;
        }
    }

    public class CancelPolicyInfo
    {
        public string versionId;
        public string cancelTime;
        public string startWindowHours;
        public string nightCount;
        public string timeZoneDescription;

        public CancelPolicyInfo(dynamic data)
        {
            versionId = data.versionId.ToString();
            cancelTime = data.cancelTime.ToString();
            startWindowHours = data.startWindowHours.ToString();
            nightCount = data.nightCount.ToString();
            timeZoneDescription = data.timeZoneDescription;
        }
    }


    public class RoomRate
    {
        public int numberOfAdults;
        public int numberOfChildren;
        public string rateKey;
        //ChargeableNightlyRates

        public RoomRate(dynamic data)
        {
            numberOfAdults = data.numberOfAdults;
            numberOfChildren = data.numberOfAdults;
            rateKey = data.rateKey;
        }
    }

    public class RateInfo
    {
        public string promo;
        public bool rateChange;
        public string cancellationPolicy;
        public List<CancelPolicyInfo> CancelPolicyInfoList;
        public bool nonRefundable;
        public string rateType;
        public int currentAllotment;
        public bool guaranteeRequired;
        public bool depositRequired;
        public decimal taxRate;

        public List<RoomRate> RoomGroup;

        public RateInfo(dynamic data)
        {
            promo = data["@promo"];
            rateChange = bool.Parse(data["@rateChange"].ToString());

            // Room group
            var arrData = (DynamicJsonArray)data.RoomGroup.Room;
            if (arrData !=null)
            {
                RoomGroup = arrData.Select(t => new RoomRate(t)).ToList();
            }
           

            cancellationPolicy = data.cancellationPolicy;

            // CancelPolicyInfoList
            //var iCP = data.CancelPolicyInfoList[]
            var arrCancel = (DynamicJsonArray)data.CancelPolicyInfoList.CancelPolicyInfo;

            CancelPolicyInfoList = arrCancel.Select(t => new CancelPolicyInfo(t)).ToList();

            nonRefundable = data.nonRefundable;
            currentAllotment = data.currentAllotment;
            rateType = data.rateType;
            guaranteeRequired = data.guaranteeRequired;
            depositRequired = data.depositRequired;
            taxRate = data.taxRate;

            

        }
    }

    public class RoomDetailDO
    {
        public string RoomTypeId { get; set; }
        public string RoomCode;
        public string RateCode { get; set; }
        public string RateDescription { get; set; }

        public int RateOccupancyPerRoom;
        public int MaxRoomOccupancy { get; set; }
        public int QuotedRoomOccupancy { get; set; }
        public string RoomDescription { get; set; }
        public string roomTypeDescription;
        public string ExpediaPropertyId { get; set; }
        public string TotalAmount { get; set; }
        public string PriceForNights { get; set; }
        public int CurrentAllotment { get; set; }

        public string DescriptionLong { get; set; }
        public List<AmenityDO> RoomAmenities { get; set; }
        public List<AmenityDO> PropertyAmenities { get; set; }

        public string checkInInstructions;

        public List<BedType> BedTypes;
        public string smokingPreferences;
        public int minGuestAge;
        public List<RateInfo> RateInfos;
        public List<ValueAdd> ValueAdds;

        //public List<HotelImage> RoomImages { get; set; }


        public string RoomBookLink { get; set; }

        public RoomDetailDO() { }

        public RoomDetailDO(dynamic data, bool IsIncludeRate = true)
        {
            RoomTypeId = data["@roomTypeId"];
            RateCode = data.rateCode.ToString();

            if (IsIncludeRate)
            {
                LoadAvailableRoomRate(data);
                DescriptionLong = data.RoomType.descriptionLong;
            }
            else
            {
                MaxRoomOccupancy = data.maxRoomOccupancy;
                QuotedRoomOccupancy = data.quotedRoomOccupancy;
                RoomDescription = data.roomDescription;
                ExpediaPropertyId = data.expediaPropertyId.ToString();
            }

            CultureInfo cul = CultureInfo.GetCultureInfo("vi-VN");   // try with "en-US"
                                                                     //(9660d / 100d, 0) *100;/
            double amnt = (double.Parse(data.RateInfos.RateInfo.ChargeableRateInfo["@total"].ToString()) * 22540);
            double amntRnd = Math.Round((amnt + 500) / 1000, 0) * 1000;
            string a = amntRnd.ToString("#,###", cul.NumberFormat);
            TotalAmount = a;

            PriceForNights = data.RateInfos.RateInfo.ChargeableRateInfo.NightlyRatesPerRoom["@size"];

            CurrentAllotment = data.RateInfos.RateInfo.currentAllotment;
        }

        public void LoadAvailableRoomRate(dynamic data)
        {
            // Room type
            RoomTypeId = data.RoomType["@roomTypeId"];
            RoomCode = data.RoomType["@roomCode"];
            RoomDescription =  data.RoomType.description;
            DescriptionLong = data.RoomType.descriptionLong;
            var arrData = (DynamicJsonArray)data.RoomType.roomAmenities.RoomAmenity;
            if (arrData != null)
            {
                RoomAmenities = arrData.Select(t => new AmenityDO(t)).ToList();
            }


            // Bed type
            var bedIdx = int.Parse(data.BedTypes["@size"]);
            if (bedIdx > 1)
            {
                var arrBed = (DynamicJsonArray)data.BedTypes;
                BedTypes = arrBed.Select(t => new BedType(t)).ToList();
            }
            else
            {
                BedTypes = new List<BedType>();
                BedTypes.Add(new BedType(data.BedTypes.BedType));
            }
            

            RateCode = data.rateCode.ToString();
            RateDescription = data.rateDescription;
            RateOccupancyPerRoom = data.rateOccupancyPerRoom;
            QuotedRoomOccupancy = data.quotedOccupancy;
            minGuestAge = data.minGuestAge;

            // Rate Info
            //var ttt = Json.Decode(data.RateInfos);
            var sRI = int.Parse(data.RateInfos["@size"]);

            if (sRI > 1)
            {
                var arrRates = (DynamicJsonArray)data.RateInfos.RateInfo;
                if (arrRates != null)
                {
                    RateInfos = arrRates.Select(t => new RateInfo(t)).ToList();
                }
            }
            else
            {
                RateInfos = new List<RateInfo>();
                RateInfos.Add(new RateInfo(data.RateInfos.RateInfo));
            }

            // Value Adds
            var arrValues = (DynamicJsonArray)data.ValueAdds.ValueAdd;
            if (arrValues !=null)
            {
                ValueAdds = arrValues.Select(t => new ValueAdd(t)).ToList();
            }

            
             
        }
    }

   
}
