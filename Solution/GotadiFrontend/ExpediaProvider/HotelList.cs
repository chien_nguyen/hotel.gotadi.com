﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace ExpediaProvider
{
    public class HotelList
    {
        public string getResponse(string url)
        {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //request.ContentType = "text/xml; charset=UTF-8";
            //request.Accept = "application/xml";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse)
            {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string temp = reader.ReadToEnd();
                reader.Close();
                response.Close();
                return temp;
            }
        }
    }
}
