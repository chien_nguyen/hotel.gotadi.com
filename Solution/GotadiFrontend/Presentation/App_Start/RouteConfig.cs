﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Presentation {
    public class RouteConfig {
        public static void RegisterRoutes(RouteCollection routes) {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");


            // Chien
            routes.MapRoute(
                name: "SearchResult",
                url: "tim-kiem/",
                defaults: new { controller = "SearchHotel", action = "SearchResult", url = UrlParameter.Optional }
            );

            // Chien
            routes.MapRoute(
                name: "HotelDetail",
                url: "khach-san/{country}/{city}/{hotel}/",
                defaults: new
                {
                    controller = "HotelDetail",
                    action = "Index",
                    country = UrlParameter.Optional,
                    city = UrlParameter.Optional,
                    hotel = UrlParameter.Optional,
                    //hotelcode = UrlParameter.Optional,
                }
            );

            // Chien
            routes.MapRoute(
                name: "InputCustomer",
                url: "thong-tin-khach/{hotelcode}/{room_type}/{room_code}",
                //url: "thong-tin-khach/{hc}/{rt}/{rc}",
                 //url: "thong-tin-khach/{*pathInfo}",
                 defaults: new { controller = "InputCustomer", action = "Index",
                    hotelcode = UrlParameter.Optional,
                    room_type = UrlParameter.Optional,
                    room_code = UrlParameter.Optional
                }
            );

            /////////////////////////////////////////////////////

            routes.MapRoute(
                name: "Pay",
                url: "pay/{hotel}/{processId}",
                defaults: new { controller = "Payment", action = "Index", hotel = UrlParameter.Optional, urlprocessId = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "HotelPlace",
                url: "khach-san-{country}/{place}",
                defaults: new
                {
                    controller = "Hotel",
                    action = "List",
                    country = UrlParameter.Optional,
                    place = UrlParameter.Optional
                }
            );

            //routes.MapRoute(
            //    name: "HotelDetail",
            //    url: "khach-san-{country}/{place}/{hotel}",
            //    defaults: new
            //    {
            //        controller = "Hotel",
            //        action = "Detail",
            //        country = UrlParameter.Optional,
            //        place = UrlParameter.Optional,
            //        hotel = UrlParameter.Optional
            //    }
            //);
            routes.MapRoute(
                name: "Default",
                url: "{*url}",
                defaults: new { controller = "Home", action = "Index", url = UrlParameter.Optional }
            );

            

            //routes.MapRoute(
            //    name: "HotelPlace",
            //    url: "khach-san-{country}/{place}",
            //    defaults: new
            //    {
            //        controller = "Hotel",
            //        action = "List",
            //        country = UrlParameter.Optional,
            //        place = UrlParameter.Optional
            //    }
            //);
        }
    }
}
