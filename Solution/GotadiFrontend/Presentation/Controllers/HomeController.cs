﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SupplierAPI;
using Presentation.Helpers;

namespace Presentation.Controllers {
    public class HomeController : Controller {
        public ActionResult Index(string url) {
            if (String.IsNullOrWhiteSpace(url)) {
                return View();
            } else {
                return View("~/Views/Error/404.cshtml");
            }
        }
    }
}