﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models;
using SupplierAPI;

namespace Presentation.Controllers {
    public class HotelController : Controller {
        public ActionResult List(string country, string place) {
            if (String.IsNullOrWhiteSpace(country) || String.IsNullOrWhiteSpace(place))
                return View("~/Views/Error/404.cshtml");
            string placeCode = Utilities.Instance.GetLastCode(place).ToUpper();
            if (String.IsNullOrWhiteSpace(placeCode))
                return View("~/Views/Error/404.cshtml");

            //Find place
            var idxPlace = (new Repositories.Places()).GetByCode(placeCode);
            if (idxPlace == null)
                return View("~/Views/Error/404.cshtml");

            //Check Cookie - Hotel Search Parameters
            var searchparams = Commons.Instance.GetCookieHotelSearchParameter();
            DateTime hotelCheckIn = searchparams.Item1;
            DateTime hotelCheckOut = searchparams.Item2;
            int hotelRooms = searchparams.Item3;
            int hotelAdult = searchparams.Item4;
            int hotelChild = searchparams.Item5;

            //Filter Parameters
            List<Metadata> filterparam = new List<Metadata>();
            int page = 1,
                rate = 0;

            string tmp = Request.QueryString["p"];
            if (!String.IsNullOrWhiteSpace(tmp))
                page = Utilities.Instance.ParseInt(tmp);
            if (page == 0) page = 1;

            tmp = Request.QueryString["s"];
            if (!String.IsNullOrWhiteSpace(tmp))
                rate = Utilities.Instance.ParseInt(tmp);
            filterparam.Add(new Metadata("s", rate.ToString()));

            //Get Hotellist from API
            HotelPro api = new HotelPro();
            var filters = HotelProElement.Instance.GetFilter("", "", rate, HotelProElement.BoardType.None, 0);
            var rooms = HotelProElement.Instance.GetRoomGuest(hotelAdult, hotelChild, hotelRooms);
            var response = api.getResponse(api.getAvailableHotel(placeCode, hotelCheckIn, hotelCheckOut, "USD", "VN", false, rooms, filters, null));
            SupplierAPI.HotelProService.getAvailableHotelResponse resultAvailableHotel = null;
            try {
                resultAvailableHotel = JsonConvert.DeserializeObject<SupplierAPI.HotelProService.getAvailableHotelResponse>(response);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                var errors = JsonConvert.DeserializeObject<List<string>>(response);
                return View("~/Views/Error/500.cshtml");
            }
            var resHotels = resultAvailableHotel.availableHotels.ToList().OrderBy(l => l.totalPrice).GroupBy(l => l.hotelCode).Select(l => l.First());

            //Filter starrate
            List<int> starrate = new List<int>() { 1, 2, 3, 4, 5 };
            List<Link> filterStar = new List<Link>();
            foreach (int i in starrate) {
                filterparam.First(x => x.key == "s").value = i.ToString();
                var o = new Link(Links.Instance.GetHotelPlace(placeCode, Utilities.Instance.ParseMetadataListToString(filterparam)), i.ToString(), "", Link.eTarget.None, Link.eRel.None);
                filterStar.Add(o);
            }
            filterparam.First(x => x.key == "s").value = rate.ToString();

            //Pagination
            Pagination pagination = new Pagination();
            pagination.totalItems = Utilities.Instance.ParseInt(resHotels.Count());
            pagination.totalPages = Utilities.Instance.GetPaginationTotal(pagination.totalItems);
            pagination.data = new List<Link>();
            var pageNumbers = Utilities.Instance.GetPaginationList(page, pagination.totalPages);
            filterparam.Add(new Metadata("p", page.ToString()));
            foreach (int i in pageNumbers) {
                filterparam.First(x => x.key == "p").value = i == 1 ? "0" : i.ToString();
                var o = new Link(Links.Instance.GetHotelPlace(placeCode, Utilities.Instance.ParseMetadataListToString(filterparam)), i.ToString(), "", Link.eTarget.None, Link.eRel.None);
                if (page == i)
                    pagination.index = o;
                pagination.data.Add(o);
            }
            filterparam.RemoveAll(x => x.key == "p");

            //Process Data
            var hotelsWS = resHotels
                            .Skip(Utilities.Instance.GetPaginationSkipItems(page))
                            .Take(Utilities.Instance.GetPaginationTakeItems()).ToList();
            var hotels = new List<HotelSlim>();
            var hotelsCode = new List<string>();
            hotelsWS.ForEach(x => hotels.Add(Hotels.Instance.mapHotelProWSToHotelSlim(x, placeCode)));
            hotels.ForEach(x => hotelsCode.Add(x.hotelCode));
            var hotelsDB = (new Repositories.Hotels()).GetByCode(hotelsCode);
            hotels = Hotels.Instance.mapHotelProDBToHotelSlim(hotels, hotelsDB);

            //View Bag - Hotel Search Parameters
            ViewBag.hdi = hotelCheckIn.ToString("dd-MM-yyyy");
            ViewBag.hdo = hotelCheckOut.ToString("dd-MM-yyyy");
            ViewBag.hrm = hotelRooms;
            ViewBag.hat = hotelAdult;
            ViewBag.hcd = hotelChild;

            //Return
            HotelList model = new HotelList();
            model.breadcrumb = null;
            model.metadata = null;
            model.index = new Place() { placeCode = idxPlace.DestinationCode, placeCountry = idxPlace.Country, placeCity = idxPlace.City };
            model.hotelItems = hotels;
            model.page = pagination;
            model.filterStarRate = filterStar;
            return View(model);
        }
        public ActionResult Detail(string country, string place, string hotel) {
            if (String.IsNullOrWhiteSpace(country) || String.IsNullOrWhiteSpace(place) || String.IsNullOrWhiteSpace(hotel))
                return View("~/Views/Error/404.cshtml");
            string placeCode = Utilities.Instance.GetLastCode(place).ToUpper();
            string hotelCode = Utilities.Instance.GetLastCode(hotel).ToUpper();
            if (String.IsNullOrWhiteSpace(placeCode) || String.IsNullOrWhiteSpace(hotelCode))
                return View("~/Views/Error/404.cshtml");

            var idxPlace = (new Repositories.Places()).GetByCode(placeCode);
            var idxHotel = (new Repositories.Hotels()).GetByCode(hotelCode);
            if (idxPlace == null || idxHotel == null)
                return View("~/Views/Error/404.cshtml");

            //Check Cookie - Hotel Search Parameters
            var searchparams = Commons.Instance.GetCookieHotelSearchParameter();
            DateTime hotelCheckIn = searchparams.Item1;
            DateTime hotelCheckOut = searchparams.Item2;
            int hotelRooms = searchparams.Item3;
            int hotelAdult = searchparams.Item4;
            int hotelChild = searchparams.Item5;

            //Get Hotellist from API
            HotelPro api = new HotelPro();
            var filters = HotelProElement.Instance.GetFilter("", "", 0, HotelProElement.BoardType.None, 0);
            var rooms = HotelProElement.Instance.GetRoomGuest(hotelAdult, hotelChild, hotelRooms);
            var response = api.getResponse(api.getAvailableHotel(placeCode, hotelCheckIn, hotelCheckOut, "USD", "VN", false, rooms, filters, null));
            SupplierAPI.HotelProService.getAvailableHotelResponse resultAvailableHotel = null;
            try {
                resultAvailableHotel = JsonConvert.DeserializeObject<SupplierAPI.HotelProService.getAvailableHotelResponse>(response);
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                var errors = JsonConvert.DeserializeObject<List<string>>(response);
                return View("~/Views/Error/500.cshtml");
            }
            var resRooms = resultAvailableHotel.availableHotels.ToList().Where(l => l.hotelCode == hotelCode).OrderBy(l => l.totalPrice).ToList();
            var fstRoom = resRooms.First();

            if (fstRoom == null)
                return View("~/Views/Error/500.cshtml");

            List<RoomDetail> listRooms = new List<RoomDetail>();
            try {
                foreach (SupplierAPI.HotelProService.hotel i in resRooms) {
                    listRooms.Add(new RoomDetail()
                    {
                        hotelProProcessId = i.processId,
                        roomPayLink = Links.Instance.GetBase("pay/" + i.processId),
                        roomCategory = i.rooms[0].roomCategory,
                        roomItems = i.rooms.Count(),
                        roomBoardType = i.boardType,
                        minPrice = Utilities.Instance.PriceFormat(i.totalPrice),
                        currency = i.currency
                    });
                }
            } catch(Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
            }

            //Get Slider Image
            var images = (new Repositories.Hotels()).GetImageByCode(hotelCode);
            List<string> slider = new List<string>();
            if(images != null)
                foreach(var i in images) {
                    if(!String.IsNullOrWhiteSpace(i.ImageURL))
                        slider.Add(i.ImageURL);
                }

            //View Bag - Hotel Search Parameters
            ViewBag.hdi = hotelCheckIn.ToString("dd-MM-yyyy");
            ViewBag.hdo = hotelCheckOut.ToString("dd-MM-yyyy");
            ViewBag.hrm = hotelRooms;
            ViewBag.hat = hotelAdult;
            ViewBag.hcd = hotelChild;

            //Return
            HotelDetail model = new HotelDetail();
            model.hotelProSearchId = resultAvailableHotel.searchId;
            model.rooms = listRooms;
            model.slider = slider;
            model.index = new HotelFull();
            model.index.hotelCode = hotelCode;
            model.index.hotelLink = Links.Instance.GetHotelDetail(placeCode, hotelCode);
            model.index.hotelName = idxHotel.HotelName;
            model.index.hotelRate = idxHotel.StarRating;
            model.index.hotelImage = idxHotel.SmallAvatarURL;
            model.index.hotelAddress = idxHotel.Address;
            model.index.hotelPromotion = "";
            model.index.geoLatitude = idxHotel.Latitude;
            model.index.geoLongitude = idxHotel.Longitude;
            model.index.minPrice = Utilities.Instance.PriceFormat(fstRoom.totalPrice);
            model.index.maxPrice = "";
            model.index.descriptionLite = idxHotel.DescriptionLite;
            model.index.currency = fstRoom.currency;
            model.index.descriptionFull = idxHotel.DescritionFull;
            if (idxHotel.PrimaryAmenities != null && idxHotel.PrimaryAmenities.Length > 0)
                model.index.amenitiesPrimary = idxHotel.PrimaryAmenities.Split(';').ToList();
            else
                model.index.amenitiesPrimary = new List<string>();
            if (idxHotel.RoomAmenities != null && idxHotel.RoomAmenities.Length > 0)
                model.index.amenitiesRoom = idxHotel.RoomAmenities.Split(';').ToList();
            else
                model.index.amenitiesRoom = new List<string>();
            return View(model);
        }
    }
}