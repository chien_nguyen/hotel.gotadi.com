﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using ExpediaModels;

namespace Presentation.Controllers
{
    public class HotelDetailController : Controller
    {
        // GET: HotelDetail
        public ActionResult Index(string country, string city, string hotel)
        {
            // Load rate type detail
            var lst = new ExpediaBusiness.RoomBL().GetRoomRateDetail();

            // Load room images
            var roomImages = new ExpediaBusiness.ImageBL().GetRoomImages("");

            ViewBag.RoomImages = roomImages;


            lst = Helpers.ReformatObjectString.FormatHotelDetail(lst, (HotelSearchCriteriaDO)Session["SEARCH_CRITERIAS"]);
            return View("~/Views/HotelDetail/HotelDetail.cshtml", lst);
        }

        [HttpPost]
        public ActionResult InputCustomer(string hotelcode, string room_type, string room_code)
        {
            return View();
        }
        
    }


}