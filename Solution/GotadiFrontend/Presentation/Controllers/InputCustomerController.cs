﻿using ExpediaModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Presentation.Controllers
{
    public class InputCustomerController : Controller
    {
        // GET: InputCustomer
        public ActionResult Index(string hc, string rt, string rc, DateTime checkin, DateTime checkout)
        {
            // Load rate type detail
            var HotelDetail = new ExpediaBusiness.RoomBL().GetRoomRateDetail();

            // Load room images
            var roomImages = new ExpediaBusiness.ImageBL().GetRoomImages("");

            ViewBag.HotelDetail = HotelDetail;
            ViewBag.RoomImages = roomImages;

            ViewBag.CheckInDay = checkin.Day;
            ViewBag.CheckOutDay = checkout.Day;

            ViewBag.CheckInMonthYear = checkin.ToString("MM/yyyy");
            ViewBag.CheckOutMonthYear = checkout.ToString("MM/yyyy");

            // Build room selector
            var room = (from r in HotelDetail.AvailableRooms
                        where r.RoomCode.Equals(rt)
                        select r).FirstOrDefault();

            List<RoomSelectorDO> RoomSelectors = new List<RoomSelectorDO>();
            var SearchCriterias = (HotelSearchCriteriaDO)Session["SEARCH_CRITERIAS"];

            foreach (var item in SearchCriterias.Rooms)
            {
                RoomSelectors.Add(new RoomSelectorDO()
                                    {
                                        RoomDetail = room,
                                    });
            }
            ViewBag.RoomSelectors = RoomSelectors;

            return View("~/Views/CustomerInfor/InputCustomerViewer.cshtml");
        }
    }
}