﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Presentation.Helpers;
using Presentation.Models;
using SupplierAPI;
using ExpediaModels;

namespace Presentation.Controllers {

    public class SearchHotelController : Controller
    {
        public ActionResult SearchResult()
        {
            List<RoomSearchCriteriaDO> roomSearchCriterias = new List<RoomSearchCriteriaDO>();
            roomSearchCriterias.Add(new RoomSearchCriteriaDO(1,2,0, ""));
            roomSearchCriterias.Add(new RoomSearchCriteriaDO(2, 2, 2, "3,7"));

            HotelSearchCriteriaDO searchCriterias = new HotelSearchCriteriaDO("Ho Chi Minh", DateTime.Parse("2015-12-12"), DateTime.Parse("2015-12-25"), roomSearchCriterias);

            Session.Add("SEARCH_CRITERIAS", searchCriterias);

            ExpediaBusiness.HotelInfoBL objEx = new ExpediaBusiness.HotelInfoBL();
            var lstObj = objEx.GetHotelList();

            var reformatedHotels = Presentation.Helpers.ReformatObjectString.FormatHotels(lstObj);

            return View(reformatedHotels);
        }
        
    }
}