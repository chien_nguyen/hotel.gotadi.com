﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Presentation.Helpers {
    public class Commons {
        static Commons _ins;
        public static Commons Instance {
            get {
                if (_ins == null) _ins = new Commons();
                return _ins;
            }
        }
        public string DateMask { get { return "yyyy-MM-dd"; } }
        public Tuple<DateTime, DateTime, int, int, int> GetCookieHotelSearchParameter() {
            bool isValid = true;
            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0),
                     defaultCheckIn = today.AddDays(1),
                     defaultCheckOut = today.AddDays(2);
            int defaultRooms = 1,
                defaultAdult = 2,
                defaultChild = 0;
            try {
                HttpCookie hsb = HttpContext.Current.Request.Cookies["hsb"];
                string hdi = "", hdo = "", hrm = "", hat = "", hcd = "";
                if (hsb == null)
                    isValid = false;
                else {
                    string[] hsbSplit = hsb.Value.Split('.');
                    hdi = hsbSplit[0];
                    hdo = hsbSplit[1];
                    hrm = hsbSplit[2];
                    hat = hsbSplit[3];
                    hcd = hsbSplit[4];
                    DateTime tryCheckIn = DateTime.Now,
                            tryCheckOut = DateTime.Now;
                    int tryRooms = 0,
                        tryAdult = 0,
                        tryChild = 0;
                    if (!DateTime.TryParseExact(hdi, Commons.Instance.DateMask, CultureInfo.InvariantCulture, DateTimeStyles.None, out tryCheckIn) ||
                        !DateTime.TryParseExact(hdo, Commons.Instance.DateMask, CultureInfo.InvariantCulture, DateTimeStyles.None, out tryCheckOut) ||
                        !Int32.TryParse(hrm, out tryRooms) ||
                        !Int32.TryParse(hat, out tryAdult) ||
                        !Int32.TryParse(hcd, out tryChild))
                        isValid = false;
                    if (tryCheckIn < today ||
                        tryCheckOut <= tryCheckIn ||
                        tryRooms <= 0 ||
                        tryAdult < tryRooms ||
                        tryChild < 0)
                        isValid = false;
                }

                if (isValid) {
                    DateTime checkIn = DateTime.ParseExact(hdi, DateMask, CultureInfo.InvariantCulture),
                        checkOut = DateTime.ParseExact(hdo, DateMask, CultureInfo.InvariantCulture);
                    int rooms = Int32.Parse(hrm),
                        adult = Int32.Parse(hat),
                        child = Int32.Parse(hcd);
                    return new Tuple<DateTime, DateTime, int, int, int>(checkIn, checkOut, rooms, adult, child);
                } else throw new Exception();
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                SetCookieHotelSearchParameter(defaultCheckIn, defaultCheckOut, defaultRooms, defaultAdult, defaultChild);
                return new Tuple<DateTime, DateTime, int, int, int>(defaultCheckIn, defaultCheckOut, defaultRooms, defaultAdult, defaultChild);
            }
        }
        public bool SetCookieHotelSearchParameter(DateTime checkIn, DateTime checkOut, int rooms, int adult, int child) {
            DateTime today = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0),
                     remarkCheckIn = new DateTime(checkIn.Year, checkIn.Month, checkIn.Day, 0, 0, 0),
                     remarkCheckOut = new DateTime(checkOut.Year, checkOut.Month, checkOut.Day, 0, 0, 0);
            if (remarkCheckIn >= today &&
                (remarkCheckOut - remarkCheckIn).Days >= 1 &&
                rooms > 0 &&
                adult >= rooms &&
                child >= 0) {
                string hsbJoin = remarkCheckIn.ToString(Commons.Instance.DateMask);
                hsbJoin += "." + remarkCheckOut.ToString(Commons.Instance.DateMask);
                hsbJoin += "." + rooms.ToString();
                hsbJoin += "." + adult.ToString();
                hsbJoin += "." + child.ToString();
                HttpCookie hsb = new HttpCookie("hsb", hsbJoin);
                hsb.Expires = DateTime.Now.AddDays(14);
                HttpContext.Current.Response.Cookies.Set(hsb);
                return true;
            }
            return false;
        }
    }
}
