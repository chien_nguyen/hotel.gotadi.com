﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.Models;
using Presentation.Repositories;
using SupplierAPI;

namespace Presentation.Helpers {
    public class Hotels {
        static Hotels _ins;
        public static Hotels Instance {
            get {
                if (_ins == null) _ins = new Hotels();
                return _ins;
            }
        }
        public HotelSlim mapHotelProWSToHotelSlim(SupplierAPI.HotelProService.hotel i, string placeCode) {
            try {
                HotelSlim o = new HotelSlim();
                o.hotelCode = i.hotelCode;
                o.hotelLink = Links.Instance.GetHotelDetail(placeCode, i.hotelCode);
                o.currency = i.currency;
                o.minPrice = Utilities.Instance.PriceFormat(i.totalPrice);
                var roomFirst = i.rooms.FirstOrDefault();
                if (roomFirst != null)
                    o.roomFirst = new RoomSlim() { roomCategory = roomFirst.roomCategory, roomItems = i.rooms.Length };
                else
                    o.roomFirst = new RoomSlim();
                return o;
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return new HotelSlim()
                {
                    roomFirst = new RoomSlim()
                };
            }
        }
        public HotelSlim mapHotelProDBToHotelSlim(HotelSlim x) {
            try {
                var res = (new Repositories.Hotels()).GetByCode(x.hotelCode);
                x.hotelName = res.HotelName;
                x.hotelRate = res.StarRating;
                x.hotelImage = res.DefaultImageURL;
                x.hotelAddress = res.Address;
                x.geoLatitude = res.Latitude;
                x.geoLongitude = res.Longitude;
                x.descriptionLite = res.DescriptionLite;
                //x.hotelPromotion;
                //x.maxPrice;
                //x.reviewCount;
                //x.reviewPoint;
                //x.reviewMessage;
                //x.badges;
                return x;
            }catch(Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return x;
            }
        }
        public HotelSlim mapHotelProDBToHotelSlim(HotelSlim x, ks_Hotels y) {
            try {
                x.hotelName = y.HotelName;
                x.hotelRate = y.StarRating;
                x.hotelImage = y.DefaultImageURL;
                x.hotelAddress = y.Address;
                x.geoLatitude = y.Latitude;
                x.geoLongitude = y.Longitude;
                x.descriptionLite = y.DescriptionLite;
                return x;
            } catch (Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return x;
            }
        }
        public List<HotelSlim> mapHotelProDBToHotelSlim(List<HotelSlim> x, List<ks_Hotels> y) {
            try {
                List<HotelSlim> z = new List<HotelSlim>();
                x.ForEach(l => z.Add(mapHotelProDBToHotelSlim(l, y.First(m => m.HotelCode == l.hotelCode))));
                return z;
            }catch(Exception e) {
                Vision.Instance.ExceptionWriteLine(e);
                return x;
            }
        }
    }
}