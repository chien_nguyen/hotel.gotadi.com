﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Helpers
{
    public class ReformatObjectString
    {
        public static ExpediaModels.Hotels FormatHotels(ExpediaModels.Hotels HotelList)
        {
            foreach (var hotel in HotelList.HotelList)
            {
                //decimal rating = decimal.Parse( item.ConfidenceRating);
                //string strRate = "";
                //if (rating >= 50)
                //{
                //    strRate = "Tốt";
                //}
                //else if (rating >= 60)
                //{
                //    strRate = "Rất Tốt";
                //}
                //else if (rating >= 70)
                //{
                //    strRate = "Tuyệt Vời";
                //}
                //else if (rating >= 90)
                //{
                //    strRate = "Rất Tuyệt";
                //}
                //item.ConfidenceRating = strRate + " " + item.ConfidenceRating;

                // Giá cho 2 đêm
                hotel.DefaultRoom.PriceForNights = "Giá cho " + hotel.DefaultRoom.PriceForNights + " đêm";

                hotel.HotelURL = Links.GetHotelURL(hotel.HotelID, hotel.HotelName, hotel.City, hotel.CountryCode);
            }

            return HotelList;
        }

        public static ExpediaModels.HotelDetailDO FormatHotelDetail(ExpediaModels.HotelDetailDO HotelDetail, ExpediaModels.HotelSearchCriteriaDO SearchCriterias)
        {
            foreach (var room in HotelDetail.AvailableRooms)
            {
                room.RoomBookLink = Links.GetBookRoomURL(HotelDetail.HotelID, room.RoomCode, room.RateCode, room.RateInfos[0].RoomGroup[0].rateKey, SearchCriterias);
            }

            return HotelDetail;
        }
    }
}