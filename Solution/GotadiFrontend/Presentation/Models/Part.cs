﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Presentation.Models {
    public class Link {
        public enum eTarget { None, _blank, _parent, _self, _top }
        public enum eRel { None, dofollow, nofollow }
        public string href { get; set; }
        public string title { get; set; }
        public string cssClass { get; set; }
        public eTarget target { get; set; }
        public eRel rel { get; set; }
        public string TargetToString {
            get {
                if (target == eTarget.None)
                    return "";
                return rel.ToString();
            }
        }
        public string RelToString {
            get {
                if (rel == eRel.None)
                    return "";
                return rel.ToString();
            }
        }
        public Link() { }
        public Link(string h, string t, string c, eTarget g, eRel r) {
            href = h;
            title = t;
            cssClass = c;
            target = g;
            rel = r;
        }
    }
    public class ImageLink : Link {
        public string src { get; set; }
        public string alt { get; set; }
        public double width { get; set; }
        public double height { get; set; }
    }
    public class SpriteBadge {
        public string tag { get; set; }
        public string tagClass { get; set; }
    }
    public class SpriteLink : Link {
        public string tag { get; set; }
        public string tagClass { get; set; }
    }
    public class Breadcrumb {
        public Link data { get; set; }
        public int level { get; set; }
    }
    public class Metadata {
        public string key { get; set; }
        public string value { get; set; }
        public Metadata() { }
        public Metadata(string k, string v) {
            key = k;
            value = v;
        }
    }
    public class Pagination {
        public int totalPages { get; set; }
        public int totalItems { get; set; }
        public Link index { get; set; }
        public List<Link> data { get; set; }
    }
}