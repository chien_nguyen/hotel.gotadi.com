﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Presentation.Models;

namespace Presentation.Repositories {
    public class Hotels {
        public Hotels() { }
        public ks_Hotels GetByCode(string code) {
            try {
                using (DataContext ctx = new DataContext()) {
                    return ctx.ks_Hotels.First(l => l.HotelCode == code);
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public List<ks_Hotels> GetByCode(List<string> code) {
            try {
                using (DataContext ctx = new DataContext()) {
                    return ctx.ks_Hotels.Where(l => code.Contains(l.HotelCode)).ToList();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
        public List<ks_HotelImages> GetImageByCode(string code) {
            try {
                using (DataContext ctx = new DataContext()) {
                    var hotelID = ctx.ks_Hotels.FirstOrDefault(l => l.HotelCode == code).HotelCode;
                    return ctx.ks_HotelImages.Where(l => l.HotelCode == hotelID.ToString()).ToList();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
                return null;
            }
        }
    }
}