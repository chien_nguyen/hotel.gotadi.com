﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierAPI {
    public class HotelPro {
        private string _apiKey = Utilities.Instance.GetConfiguration("hotelpro-apikey");
        private string _apiUrl = Utilities.Instance.GetConfiguration("hotelpro-url");
        private string _defaultChildAge = "6";
        public HotelPro() { }
        public string getResponse(string url) {
            HttpWebRequest request = WebRequest.Create(url) as HttpWebRequest;
            //request.ContentType = "text/xml; charset=UTF-8";
            //request.Accept = "application/xml";
            using (HttpWebResponse response = request.GetResponse() as HttpWebResponse) {
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string temp = reader.ReadToEnd();
                reader.Close();
                response.Close();
                return temp;
            }
        }
        public string getResponse(Dictionary<string, string> request) {
            return getResponse(dictionaryToString(request));
        }
        public string dictionaryToString(Dictionary<string, string> dict) {
            if (dict == null)
                return string.Empty;
            if (string.IsNullOrEmpty(dict["method"]))
                return string.Empty;
            return string.Format(_apiUrl + "?{0}",
                    string.Join("&",
                        dict.Select(kvp =>
                            string.Format("{0}={1}", kvp.Key, kvp.Value))));
        }
        public Dictionary<string, string> getAvailableHotel(string destinationId, DateTime checkIn, DateTime checkOut, string currency,
            string clientNationality, bool onRequest, HotelProService.pax[][] rooms, HotelProService.filter[] filters,
            HotelProService.paginationRequestSet paginationRequest) {
            var dict = new Dictionary<string, string>()
            {
                { "method" , "getAvailableHotel" },
                { "apiKey" , _apiKey },
                { "destinationId", destinationId },
                { "checkIn", checkIn.ToString("yyyy-MM-dd") },
                { "checkOut", checkOut.ToString("yyyy-MM-dd") },
                { "currency", currency },
                { "clientNationality", clientNationality },
                { "onRequest", onRequest ? "true" : "false" }
            };
            if(rooms.Length > 0) {
                for(int i = 0; i < rooms.Length; i++) {
                    for(int j = 0; j < rooms[i].Length; j++) {
                        dict.Add("rooms[" + i + "][" + j + "][paxType]", rooms[i][j].paxType);
                        if(rooms[i][j].paxType == "Child") {
                            dict.Add("rooms[" + i + "][" + j + "][age]", String.IsNullOrWhiteSpace(rooms[i][j].age) ? _defaultChildAge : rooms[i][j].age);
                        }
                    }
                }
            }
            if (filters.Length > 0) {
                for (int i = 0; i < filters.Length; i++) {
                    dict.Add("filters[" + i + "][filterType]", filters[i].filterType);
                    dict.Add("filters[" + i + "][filterValue]", filters[i].filterValue);
                }
            }
            return dict;
        }
        public Dictionary<string, string> allocateHotelCode(string searchId, string hotelCode) {
            return new Dictionary<string, string>()
            {
                { "method" , "allocateHotelCode" },
                { "apiKey" , _apiKey },
                { "searchId", searchId },
                { "hotelCode", hotelCode }
            };
        }
        public Dictionary<string, string> makeHotelBooking(string processId, string agencyReferenceNumber, 
            HotelProService.leadTraveller leadTravellerInfo, HotelProService.pax[] otherTravellerInfo, 
            string preferences, string note, string hotelCode) {
            var dict = new Dictionary<string, string>()
            {
                { "method" , "makeHotelBooking" },
                { "apiKey" , _apiKey },
                { "processId", processId },
                { "agencyReferenceNumber", agencyReferenceNumber },
                { "leadTravellerInfo[paxInfo][paxType]",leadTravellerInfo.paxInfo.paxType },
                { "leadTravellerInfo[paxInfo][title]", leadTravellerInfo.paxInfo.title },
                { "leadTravellerInfo[paxInfo][firstName]", leadTravellerInfo.paxInfo.firstName },
                { "leadTravellerInfo[paxInfo][lastName]", leadTravellerInfo.paxInfo.lastName },
                { "leadTravellerInfo[nationality]", leadTravellerInfo.nationality },
                { "preferences", preferences },
                { "note", note },
                { "hotelCode", hotelCode }
            };
            for(int i = 0; i < otherTravellerInfo.Length; i++) {
                dict.Add("otherTravellerInfo[" + i + "][title]", otherTravellerInfo[i].title);
                dict.Add("otherTravellerInfo[" + i + "][firstName]", otherTravellerInfo[i].firstName);
                dict.Add("otherTravellerInfo[" + i + "][lastName]", otherTravellerInfo[i].lastName);
            }
            return dict;
        }
        public Dictionary<string, string> getHotelBookingList(DateTime bookDateFrom, DateTime bookDateTo, DateTime checkInFrom, DateTime checkInTo,
            DateTime checkOutFrom, DateTime checkOutTo, string bookingStatus) {
            return new Dictionary<string, string>()
            {
                { "method" , "getHotelBookingList" },
                { "apiKey" , _apiKey },
                { "bookDateFrom" , bookDateFrom.ToString("yyyy-MM-dd") },
                { "bookDateTo" , bookDateTo.ToString("yyyy-MM-dd") },
                { "checkInFrom" , checkInFrom.ToString("yyyy-MM-dd") },
                { "checkInTo" , checkInTo.ToString("yyyy-MM-dd") },
                { "checkOutFrom" , checkOutFrom.ToString("yyyy-MM-dd") },
                { "checkOutTo" , checkOutTo.ToString("yyyy-MM-dd") },
                { "bookingStatus" , bookingStatus }
            };
        }
        public Dictionary<string, string> getHotelBookingStatus(string trackingId) {
            return new Dictionary<string, string>()
            {
                { "method" , "getHotelBookingStatus" },
                { "apiKey" , _apiKey },
                { "trackingId", trackingId }
            };
        }
        public Dictionary<string, string> cancelHotelBooking(string trackingId) {
            return new Dictionary<string, string>()
            {
                { "method" , "cancelHotelBooking" },
                { "apiKey" , _apiKey },
                { "trackingId", trackingId }
            };
        }
        public Dictionary<string, string> getHotelCancellationPolicy(string trackingId, string hotelCode) {
            return new Dictionary<string, string>()
            {
                { "method" , "getHotelCancellationPolicy" },
                { "apiKey" , _apiKey },
                { "trackingId", trackingId },
                { "hotelCode", hotelCode }
            };
        }
        public Dictionary<string, string> amendHotelBooking(string trackingId, 
            DateTime checkIn, DateTime checkOut, HotelProService.leadTraveller leadTravellerInfo, 
            HotelProService.pax[][] rooms, string preferences, string note) {
            var a = new HotelProService.leadTraveller();
            var dict = new Dictionary<string, string>()
            {
                { "method" , "amendHotelBooking" },
                { "apiKey" , _apiKey },
                { "trackingId" , trackingId },
                { "checkIn", checkIn.ToString("yyyy-MM-dd") },
                { "checkOut", checkOut.ToString("yyyy-MM-dd") },
                { "leadTravellerInfo[paxInfo][paxType]",leadTravellerInfo.paxInfo.paxType },
                { "leadTravellerInfo[paxInfo][title]", leadTravellerInfo.paxInfo.title },
                { "leadTravellerInfo[paxInfo][firstName]", leadTravellerInfo.paxInfo.firstName },
                { "leadTravellerInfo[paxInfo][lastName]", leadTravellerInfo.paxInfo.lastName },
                { "leadTravellerInfo[nationality]", leadTravellerInfo.nationality },
                { "preferences" , preferences },
                { "note" , note }
            };
            for(int i = 0; i < rooms.Length; i++) {
                for(int j = 0; j < rooms[i].Length; j++) {
                    dict.Add("rooms[" + i + "][" + j + "][paxType]", rooms[i][j].paxType);
                    dict.Add("rooms[" + i + "][" + j + "][title]", rooms[i][j].title);
                    dict.Add("rooms[" + i + "][" + j + "][firstName]", rooms[i][j].firstName);
                    dict.Add("rooms[" + i + "][" + j + "][lastName]", rooms[i][j].lastName);
                    if (rooms[i][j].paxType == "Child") {
                        dict.Add("rooms[" + i + "][" + j + "][age]", String.IsNullOrWhiteSpace(rooms[i][j].age) ? _defaultChildAge : rooms[i][j].age);
                    }
                }
            }
            return dict;
        }
        public Dictionary<string, string> getBalance() {
            return new Dictionary<string, string>()
            {
                { "method" , "getBalance" },
                { "apiKey" , _apiKey }
            };
        }
    }
}
