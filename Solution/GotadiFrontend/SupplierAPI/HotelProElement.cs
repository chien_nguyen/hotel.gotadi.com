﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SupplierAPI {
    public class HotelProElement {
        static HotelProElement _instance;
        public static HotelProElement Instance {
            get {
                if (_instance == null) _instance = new HotelProElement();
                return _instance;
            }
        }
        public enum BoardType { RO, BB, EB, CB, HB, FB, AI, UI, SC, None };
        public enum GuestType { Adult, Child }
        public HotelProService.filter[] GetFilter(string hotelCode, string hotelName, int hotelStar, BoardType boardType, int resultLimit) {
            if (hotelStar < 1)
                hotelStar = 0;
            else if (hotelStar > 5)
                hotelStar = 0;

            if (hotelName.Length < 3)
                hotelName = "";

            var filters = new List<HotelProService.filter>();
            if (hotelCode.Length > 0)
                filters.Add(new HotelProService.filter { filterType = "hotelCode", filterValue = hotelCode });
            if (hotelStar != 0)
                filters.Add(new HotelProService.filter { filterType = "hotelStar", filterValue = hotelStar.ToString() });
            if (hotelName.Length > 0)
                filters.Add(new HotelProService.filter { filterType = "hotelName", filterValue = hotelName });
            if (boardType != BoardType.None)
                filters.Add(new HotelProService.filter { filterType = "boardType", filterValue = boardType.ToString() });
            if (resultLimit > 0)
                filters.Add(new HotelProService.filter { filterType = "resultLimit", filterValue = resultLimit.ToString() });
            return filters.ToArray();
        }
        public HotelProService.pax[][] GetRoomGuest(int adult, int child, int room) {
            if (adult < child) return (new List<HotelProService.pax[]>()).ToArray();
            int adultstandard = adult / room,
                adultmodulo = adult % room,
                childstandard = child / room,
                childmodulo = child % room;
            var rooms = new List<HotelProService.pax[]>();
            for (int i = 0; i < room; i++) {
                var tmp = new List<HotelProService.pax>();
                for(int j = 0; j < adultstandard + (i < adultmodulo ? 1 : 0); j++) {
                    tmp.Add(new HotelProService.pax { paxType = GuestType.Adult.ToString() });
                }
                for (int j = 0; j < childstandard + (i < childmodulo ? 1 : 0); j++) {
                    tmp.Add(new HotelProService.pax { paxType = GuestType.Child.ToString() });
                }
                rooms.Add(tmp.ToArray());
            }            
            return rooms.ToArray();
        }
        public string MapBoardType(BoardType boardType) {
            switch (boardType) {
                case BoardType.RO:
                    return "Room Only";
                case BoardType.BB:
                    return "Bed and Breakfast";
                case BoardType.EB:
                    return "English Breakfast";
                case BoardType.CB:
                    return "Continential Breakfast";
                case BoardType.HB:
                    return "Half Board";
                case BoardType.FB:
                    return "Full Board";
                case BoardType.AI:
                    return "All Inclusive";
                case BoardType.UI:
                    return "Ultra All Inclusive";
                case BoardType.SC:
                    return "Self Catering";
                default:
                    return "";
            }
        }
    }
}
