﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading.Tasks;

namespace SupplierAPI {
    class Utilities {
        static Utilities _instance;
        public static Utilities Instance {
            get {
                if (_instance == null) _instance = new Utilities();
                return _instance;
            }
        }
        public string GetConfiguration(string key) {
            if (string.IsNullOrEmpty(key))
                return String.Empty;
            try {
                return ConfigurationManager.AppSettings[key];
            } catch (Exception ex) {
                throw ex;
            }
        }
    }
}
